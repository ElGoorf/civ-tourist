import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { Grid } from '@material-ui/core';
import Head from 'next/head';
import WonderGallery from '../components/WonderGallery';
import Results from '../components/Results';
import Footer from '../components/Footer';

export default () => {
  const [wonderSelections, setSelected] = React.useState({});
  const [resultsVisible, showResults] = React.useState(false);

  const handleChange = (name) => (checked) => {
    setSelected({ ...wonderSelections, [name]: checked });
  };

  const submitForm = (e) => {
    e.preventDefault();
    showResults(true);
    setTimeout(() => {
      document.querySelector('#result').scrollIntoView({
        behavior: 'smooth',
      });
    }, 100);
  };

  return (
    <>
      <Head>
        <title>Civ Tourist</title>
      </Head>
      <CssBaseline />
      <Container maxWidth="xl" fixed>
        <header>
          <Typography variant="h1" style={{ fontSize: '4rem' }}>Civ Tourist</Typography>
        </header>
        <div style={{ minHeight: 'calc(100vh - 80px)' }}>
          {/* Part 1 */}
          <section style={{ minHeight: '100%' }}>
            <Typography
              variant="subtitle1"
              style={{ margin: '10px 0', lineHeight: '1.4em' }}
            >
              <strong>Which of these places have you visited?</strong> Select the places you've been to, or any others, it doesn't matter, this is all just a bit of fun :)
            </Typography>
            <form onSubmit={submitForm}>
              <WonderGallery handleChange={handleChange} />
              <Grid container direction="row" justify="flex-end">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  endIcon={<NavigateNextIcon />}
                  style={{ margin: '20px' }}
                >
                  Next
                </Button>
              </Grid>
            </form>
          </section>
          {/* Part 2 */}
          <section style={{ minHeight: 'calc(100vh - 110px)', display: resultsVisible ? 'block' : 'none' }} id="result">
            <hr />
            <Results wonderSelections={wonderSelections} />
          </section>
        </div>
        <Footer />
      </Container>
    </>
  );
};
