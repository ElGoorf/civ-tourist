import buildings from '../Buildings';
import names from '../Types_Text';
import replaceDiacritics from '../../util/replaceDiacritics';

function getDataFor(wonder, rawFieldName, fieldKeyKey, fieldValueKey) {
  const wonderBuildingType = wonder['-BuildingType'];
  return buildings.GameInfo[rawFieldName].Row
    .filter((dataSet) => (
      dataSet['-BuildingType'] === wonderBuildingType
    ))
    .reduce((acc, dataSet) => ({
      ...acc,
      [dataSet[fieldKeyKey]]: parseInt(dataSet[fieldValueKey], 10),
    }), {});
}

function getYieldsFor(wonder) {
  return getDataFor(wonder, 'Building_YieldChanges', '-YieldType', '-YieldChange');
}

function getGreatPersonPointsFor(wonder) {
  return getDataFor(wonder, 'Building_GreatPersonPoints', '-GreatPersonClassType', '-PointsPerTurn');
}

function getGreatWorksSlotsFor(wonder) {
  return getDataFor(wonder, 'Building_GreatWorks', '-GreatWorkSlotType', '-NumSlots');
}

function getModifiers(wonder) {
  const wonderBuildingType = wonder['-BuildingType'];
  const applicableModifiers = buildings.GameInfo.BuildingModifiers.Row
    .filter((buildingModifier) => (buildingModifier.BuildingType || buildingModifier['-BuildingType']) === wonderBuildingType)
    .reduce((acc, buildingModifier) => {
      const { ModifierId } = buildingModifier;
      const modifierArguments = buildings.GameInfo.ModifierArguments.Row
        .filter((modifierArgument) => modifierArgument.ModifierId === ModifierId);

      return {
        ...acc,
        [ModifierId]: modifierArguments,
      };
    }, {});

  return applicableModifiers;
}

const wonderList: ProcessedWonder[] = buildings.GameInfo.Buildings.Row
  .filter((building) => building['-IsWonder'] === 'true')
  .map((wonder) => {
    const name = names.GameData.BaseGameText.Row.find((building) => building['-Tag'] === wonder['-Name']).Text;
    const imgSrc = `/icons/wonders/__SIZE__/Icon_building_${replaceDiacritics(name).toLowerCase()
      .replace(/ /g, '_')
      .replace(/\./g, '')
    }`;

    return {
      name,
      imgSrc,
      yields: getYieldsFor(wonder),
      greatPersonPoints: getGreatPersonPointsFor(wonder),
      greatWorksSlots: getGreatWorksSlotsFor(wonder),
      modifiers: getModifiers(wonder),
    };
  })
  .sort((wonderA, wonderB) => {
    if (wonderA.name < wonderB.name) { return -1; }
    if (wonderA.name > wonderB.name) { return 1; }
    return 0;
  });

export default wonderList;
