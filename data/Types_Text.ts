// Raw data taken from `...`\Sid Meier's Civilization VI\Base\Assets\Text\en_US\Types_Text.xml` and
// converted to JSON

export default {
  GameData: {
    BaseGameText: {
      Row: [
        {
          '-Tag': 'LOC_BELIEF_NAME',
          Text: 'Belief',
        },
        {
          '-Tag': 'LOC_BUILDING_NAME',
          Text: 'Building',
        },
        {
          '-Tag': 'LOC_BUILDING_NAME_REPLACES',
          Text: 'Building (replaces {1_BuildingName})',
        },
        {
          '-Tag': 'LOC_CIVIC_NAME',
          Text: 'Civic',
        },
        {
          '-Tag': 'LOC_DISTRICT_NAME',
          Text: 'District',
        },
        {
          '-Tag': 'LOC_DISTRICT_NAME_REPLACES',
          Text: 'District (replaces {1_DistrictName})',
        },
        {
          '-Tag': 'LOC_ENVOY_NAME',
          Text: 'Envoy',
        },
        {
          '-Tag': 'LOC_ERA_NAME',
          Text: 'Era',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_NAME',
          Text: 'Government',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_NAME',
          Text: 'Improvement',
        },
        {
          '-Tag': 'LOC_POLICY_NAME',
          Text: 'Policy',
        },
        {
          '-Tag': 'LOC_PROJECT_NAME',
          Text: 'Project',
        },
        {
          '-Tag': 'LOC_RESOURCE_NAME',
          Text: 'Resource',
        },
        {
          '-Tag': 'LOC_ROUTE_NAME',
          Text: 'Route',
        },
        {
          '-Tag': 'LOC_SPY_NAME',
          Text: 'Spy',
        },
        {
          '-Tag': 'LOC_TECHNOLOGY_NAME',
          Text: 'Technology',
        },
        {
          '-Tag': 'LOC_UNIT_NAME',
          Text: 'Unit',
        },
        {
          '-Tag': 'LOC_UNIT_NAME_REPLACES',
          Text: 'Unit (replaces {1_UnitName})',
        },
        {
          '-Tag': 'LOC_WONDER_NAME',
          Text: 'Wonder',
        },
        {
          '-Tag': 'LOC_WONDER_NAME_REPLACES',
          Text: 'Wonder (replaces {1_WonderName})',
        },
        {
          '-Tag': 'LOC_NATURAL_WONDER_NAME',
          Text: 'Natural Wonder',
        },
        {
          '-Tag': 'LOC_BUILDING_AIRPORT_NAME',
          Text: 'Airport',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_ALHAMBRA_NAME',
          Text: 'Alhambra',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_AMPHITHEATER_NAME',
          Text: 'Amphitheater',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_ARENA_NAME',
          Text: 'Arena',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_ARMORY_NAME',
          Text: 'Armory',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_BANK_NAME',
          Text: 'Bank',
        },
        {
          '-Tag': 'LOC_BUILDING_BARRACKS_NAME',
          Text: 'Barracks',
        },
        {
          '-Tag': 'LOC_BUILDING_BIG_BEN_NAME',
          Text: 'Big Ben',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_BOLSHOI_THEATRE_NAME',
          Text: 'Bolshoi Theatre',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_BROADCAST_CENTER_NAME',
          Text: 'Broadcast Center',
        },
        {
          '-Tag': 'LOC_BUILDING_BROADWAY_NAME',
          Text: 'Broadway',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_BUILDINGS',
          Text: 'Buildings',
        },
        {
          '-Tag': 'LOC_BUILDING_CASTLE_NAME',
          Text: 'Medieval Walls',
        },
        {
          '-Tag': 'LOC_BUILDING_CATHEDRAL_NAME',
          Text: 'Cathedral',
        },
        {
          '-Tag': 'LOC_BUILDING_CHICHEN_ITZA_NAME',
          Text: 'Chichen Itza',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_COLOSSEUM_NAME',
          Text: 'Colosseum',
        },
        {
          '-Tag': 'LOC_BUILDING_COLOSSUS_NAME',
          Text: 'Colossus',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_CRISTO_REDENTOR_NAME',
          Text: 'Cristo Redentor',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_DAR_E_MEHR_NAME',
          Text: 'Dar-e Mehr',
        },
        {
          '-Tag': 'LOC_BUILDING_EIFFEL_TOWER_NAME',
          Text: 'Eiffel Tower',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_ELECTRONICS_FACTORY_NAME',
          Text: 'Electronics Factory',
        },
        {
          '-Tag': 'LOC_BUILDING_ESTADIO_DO_MARACANA_NAME',
          Text: 'Estádio do Maracanã',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_FACTORY_NAME',
          Text: 'Factory',
        },
        {
          '-Tag': 'LOC_BUILDING_FILM_STUDIO_NAME',
          Text: 'Film Studio',
        },
        {
          '-Tag': 'LOC_BUILDING_FORBIDDEN_CITY_NAME',
          Text: 'Forbidden City',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_GRANARY_NAME',
          Text: 'Granary',
        },
        {
          '-Tag': 'LOC_BUILDING_GREAT_LIBRARY_NAME',
          Text: 'Great Library',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_GREAT_LIGHTHOUSE_NAME',
          Text: 'Great Lighthouse',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_GREAT_ZIMBABWE_NAME',
          Text: 'Great Zimbabwe',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_GURDWARA_NAME',
          Text: 'Gurdwara',
        },
        {
          '-Tag': 'LOC_BUILDING_HAGIA_SOPHIA_NAME',
          Text: 'Hagia Sophia',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_HANGAR_NAME',
          Text: 'Hangar',
        },
        {
          '-Tag': 'LOC_BUILDING_HANGING_GARDENS_NAME',
          Text: 'Hanging Gardens',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_HERMITAGE_NAME',
          Text: 'Hermitage',
        },
        {
          '-Tag': 'LOC_BUILDING_LARGE_ROCKET_NAME',
          Text: 'Large Rocket',
        },
        {
          '-Tag': 'LOC_BUILDING_LIBRARY_NAME',
          Text: 'Library',
        },
        {
          '-Tag': 'LOC_BUILDING_LIGHTHOUSE_NAME',
          Text: 'Lighthouse',
        },
        {
          '-Tag': 'LOC_BUILDING_MADRASA_NAME',
          Text: 'Madrasa',
        },
        {
          '-Tag': 'LOC_BUILDING_MAHABODHI_TEMPLE_NAME',
          Text: 'Mahabodhi Temple',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_MARKET_NAME',
          Text: 'Market',
        },
        {
          '-Tag': 'LOC_BUILDING_MEDIUM_ROCKET_NAME',
          Text: 'Medium Rocket',
        },
        {
          '-Tag': 'LOC_BUILDING_MEETING_HOUSE_NAME',
          Text: 'Meeting House',
        },
        {
          '-Tag': 'LOC_BUILDING_MILITARY_ACADEMY_NAME',
          Text: 'Military Academy',
        },
        {
          '-Tag': 'LOC_BUILDING_MONT_ST_MICHEL_NAME',
          Text: 'Mont St. Michel',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_MONUMENT_NAME',
          Text: 'Monument',
        },
        {
          '-Tag': 'LOC_BUILDING_MOSQUE_NAME',
          Text: 'Mosque',
        },
        {
          '-Tag': 'LOC_BUILDING_MUSEUM_ART_NAME',
          Text: 'Art Museum',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_MUSEUM_ARTIFACT_NAME',
          Text: 'Archaeological Museum',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_OBSERVATORY',
          Text: 'Observatory',
        },
        {
          '-Tag': 'LOC_BUILDING_ORACLE_NAME',
          Text: 'Oracle',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_BUILDING_OXFORD_UNIVERSITY_NAME',
          Text: 'Oxford University',
        },
        {
          '-Tag': 'LOC_BUILDING_PAGODA_NAME',
          Text: 'Pagoda',
        },
        {
          '-Tag': 'LOC_BUILDING_PALACE_NAME',
          Text: 'Palace',
        },
        {
          '-Tag': 'LOC_BUILDING_PETRA_NAME',
          Text: 'Petra',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_POTALA_PALACE_NAME',
          Text: 'Potala Palace',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_POWER_PLANT_NAME',
          Text: 'Power Plant',
        },
        {
          '-Tag': 'LOC_BUILDING_PYRAMIDS_NAME',
          Text: 'Pyramids',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_RESEARCH_LAB_NAME',
          Text: 'Research Lab',
        },
        {
          '-Tag': 'LOC_BUILDING_RUHR_VALLEY_NAME',
          Text: 'Ruhr Valley',
          Gender: 'Masculine:no_article',
        },
        {
          '-Tag': 'LOC_BUILDING_SEAPORT_NAME',
          Text: 'Seaport',
        },
        {
          '-Tag': 'LOC_BUILDING_SEWER_NAME',
          Text: 'Sewer',
        },
        {
          '-Tag': 'LOC_BUILDING_SHIPYARD_NAME',
          Text: 'Shipyard',
        },
        {
          '-Tag': 'LOC_BUILDING_SHRINE_NAME',
          Text: 'Shrine',
        },
        {
          '-Tag': 'LOC_BUILDING_SMALL_ROCKET_NAME',
          Text: 'Small Rocket',
        },
        {
          '-Tag': 'LOC_BUILDING_STABLE_NAME',
          Text: 'Stable',
        },
        {
          '-Tag': 'LOC_BUILDING_STADIUM_NAME',
          Text: 'Stadium',
        },
        {
          '-Tag': 'LOC_BUILDING_STAR_FORT_NAME',
          Text: 'Renaissance Walls',
        },
        {
          '-Tag': 'LOC_BUILDING_STAVE_CHURCH_NAME',
          Text: 'Stave Church',
        },
        {
          '-Tag': 'LOC_BUILDING_STOCK_EXCHANGE_NAME',
          Text: 'Stock Exchange',
        },
        {
          '-Tag': 'LOC_BUILDING_STONEHENGE_NAME',
          Text: 'Stonehenge',
        },
        {
          '-Tag': 'LOC_BUILDING_STUPA_NAME',
          Text: 'Stupa',
        },
        {
          '-Tag': 'LOC_BUILDING_SYDNEY_OPERA_HOUSE_NAME',
          Text: 'Sydney Opera House',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_SYNAGOGUE_NAME',
          Text: 'Synagogue',
        },
        {
          '-Tag': 'LOC_BUILDING_TEMPLE_NAME',
          Text: 'Temple',
        },
        {
          '-Tag': 'LOC_BUILDING_TERRACOTTA_ARMY_NAME',
          Text: 'Terracotta Army',
          Gender: 'Masculine:the',
        },
        {
          '-Tag': 'LOC_BUILDING_UNIVERSITY_NAME',
          Text: 'University',
        },
        {
          '-Tag': 'LOC_BUILDING_VENETIAN_ARSENAL_NAME',
          Text: 'Venetian Arsenal',
        },
        {
          '-Tag': 'LOC_BUILDING_WALLS_NAME',
          Text: 'Ancient Walls',
        },
        {
          '-Tag': 'LOC_BUILDING_WAT_NAME',
          Text: 'Wat',
        },
        {
          '-Tag': 'LOC_BUILDING_WATER_MILL_NAME',
          Text: 'Water Mill',
        },
        {
          '-Tag': 'LOC_BUILDING_WORKSHOP_NAME',
          Text: 'Workshop',
        },
        {
          '-Tag': 'LOC_BUILDING_ZOO_NAME',
          Text: 'Zoo',
        },
        {
          '-Tag': 'LOC_CIVIC_CODE_OF_LAWS_NAME',
          Text: 'Code of Laws',
        },
        {
          '-Tag': 'LOC_CIVIC_CRAFTSMANSHIP_NAME',
          Text: 'Craftsmanship',
        },
        {
          '-Tag': 'LOC_CIVIC_FOREIGN_TRADE_NAME',
          Text: 'Foreign Trade',
        },
        {
          '-Tag': 'LOC_CIVIC_MILITARY_TRADITION_NAME',
          Text: 'Military Tradition',
        },
        {
          '-Tag': 'LOC_CIVIC_STATE_WORKFORCE_NAME',
          Text: 'State Workforce',
        },
        {
          '-Tag': 'LOC_CIVIC_EARLY_EMPIRE_NAME',
          Text: 'Early Empire',
        },
        {
          '-Tag': 'LOC_CIVIC_MYSTICISM_NAME',
          Text: 'Mysticism',
        },
        {
          '-Tag': 'LOC_CIVIC_GAMES_RECREATION_NAME',
          Text: 'Games and Recreation',
        },
        {
          '-Tag': 'LOC_CIVIC_POLITICAL_PHILOSOPHY_NAME',
          Text: 'Political Philosophy',
        },
        {
          '-Tag': 'LOC_CIVIC_DRAMA_POETRY_NAME',
          Text: 'Drama and Poetry',
        },
        {
          '-Tag': 'LOC_CIVIC_MILITARY_TRAINING_NAME',
          Text: 'Military Training',
        },
        {
          '-Tag': 'LOC_CIVIC_DEFENSIVE_TACTICS_NAME',
          Text: 'Defensive Tactics',
        },
        {
          '-Tag': 'LOC_CIVIC_RECORDED_HISTORY_NAME',
          Text: 'Recorded History',
        },
        {
          '-Tag': 'LOC_CIVIC_THEOLOGY_NAME',
          Text: 'Theology',
        },
        {
          '-Tag': 'LOC_CIVIC_NAVAL_TRADITION_NAME',
          Text: 'Naval Tradition',
        },
        {
          '-Tag': 'LOC_CIVIC_FEUDALISM_NAME',
          Text: 'Feudalism',
        },
        {
          '-Tag': 'LOC_CIVIC_CIVIL_SERVICE_NAME',
          Text: 'Civil Service',
        },
        {
          '-Tag': 'LOC_CIVIC_MERCENARIES_NAME',
          Text: 'Mercenaries',
        },
        {
          '-Tag': 'LOC_CIVIC_MEDIEVAL_FAIRES_NAME',
          Text: 'Medieval Faires',
        },
        {
          '-Tag': 'LOC_CIVIC_GUILDS_NAME',
          Text: 'Guilds',
        },
        {
          '-Tag': 'LOC_CIVIC_DIVINE_RIGHT_NAME',
          Text: 'Divine Right',
        },
        {
          '-Tag': 'LOC_CIVIC_EXPLORATION_NAME',
          Text: 'Exploration',
        },
        {
          '-Tag': 'LOC_CIVIC_HUMANISM_NAME',
          Text: 'Humanism',
        },
        {
          '-Tag': 'LOC_CIVIC_DIPLOMATIC_SERVICE_NAME',
          Text: 'Diplomatic Service',
        },
        {
          '-Tag': 'LOC_CIVIC_REFORMED_CHURCH_NAME',
          Text: 'Reformed Church',
        },
        {
          '-Tag': 'LOC_CIVIC_MERCANTILISM_NAME',
          Text: 'Mercantilism',
        },
        {
          '-Tag': 'LOC_CIVIC_THE_ENLIGHTENMENT_NAME',
          Text: 'The Enlightenment',
        },
        {
          '-Tag': 'LOC_CIVIC_COLONIALISM_NAME',
          Text: 'Colonialism',
        },
        {
          '-Tag': 'LOC_CIVIC_CIVIL_ENGINEERING_NAME',
          Text: 'Civil Engineering',
        },
        {
          '-Tag': 'LOC_CIVIC_NATIONALISM_NAME',
          Text: 'Nationalism',
        },
        {
          '-Tag': 'LOC_CIVIC_OPERA_BALLET_NAME',
          Text: 'Opera and Ballet',
        },
        {
          '-Tag': 'LOC_CIVIC_NATURAL_HISTORY_NAME',
          Text: 'Natural History',
        },
        {
          '-Tag': 'LOC_CIVIC_URBANIZATION_NAME',
          Text: 'Urbanization',
        },
        {
          '-Tag': 'LOC_CIVIC_SCORCHED_EARTH_NAME',
          Text: 'Scorched Earth',
        },
        {
          '-Tag': 'LOC_CIVIC_CONSERVATION_NAME',
          Text: 'Conservation',
        },
        {
          '-Tag': 'LOC_CIVIC_MASS_MEDIA_NAME',
          Text: 'Mass Media',
        },
        {
          '-Tag': 'LOC_CIVIC_MOBILIZATION_NAME',
          Text: 'Mobilization',
        },
        {
          '-Tag': 'LOC_CIVIC_CAPITALISM_NAME',
          Text: 'Capitalism',
        },
        {
          '-Tag': 'LOC_CIVIC_NUCLEAR_PROGRAM_NAME',
          Text: ' Nuclear Program',
        },
        {
          '-Tag': 'LOC_CIVIC_IDEOLOGY_NAME',
          Text: 'Ideology',
        },
        {
          '-Tag': 'LOC_CIVIC_SUFFRAGE_NAME',
          Text: 'Suffrage',
        },
        {
          '-Tag': 'LOC_CIVIC_TOTALITARIANISM_NAME',
          Text: 'Totalitarianism',
        },
        {
          '-Tag': 'LOC_CIVIC_CLASS_STRUGGLE_NAME',
          Text: 'Class Struggle',
        },
        {
          '-Tag': 'LOC_CIVIC_COLD_WAR_NAME',
          Text: 'Cold War',
        },
        {
          '-Tag': 'LOC_CIVIC_PROFESSIONAL_SPORTS_NAME',
          Text: 'Professional Sports',
        },
        {
          '-Tag': 'LOC_CIVIC_CULTURAL_HERITAGE_NAME',
          Text: 'Cultural Heritage',
        },
        {
          '-Tag': 'LOC_CIVIC_RAPID_DEPLOYMENT_NAME',
          Text: 'Rapid Deployment',
        },
        {
          '-Tag': 'LOC_CIVIC_SPACE_RACE_NAME',
          Text: 'Space Race',
        },
        {
          '-Tag': 'LOC_CIVIC_SOCIAL_MEDIA_NAME',
          Text: 'Social Media',
        },
        {
          '-Tag': 'LOC_CIVIC_GLOBALIZATION_NAME',
          Text: 'Globalization',
        },
        {
          '-Tag': 'LOC_CIVIC_FUTURE_CIVIC_NAME',
          Text: 'Future Civic',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_AMERICA_NAME',
          Text: 'America',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_ARABIA_NAME',
          Text: 'Arabia',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_BARBARIAN_NAME',
          Text: 'Barbarians',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_BRAZIL_NAME',
          Text: 'Brazil',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_CHINA_NAME',
          Text: 'China',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_EGYPT_NAME',
          Text: 'Egypt',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_ENGLAND_NAME',
          Text: 'England',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_FRANCE_NAME',
          Text: 'France',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_GERMANY_NAME',
          Text: 'Germany',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_GREECE_NAME',
          Text: 'Greece',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_INDIA_NAME',
          Text: 'India',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_JAPAN_NAME',
          Text: 'Japan',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_KONGO_NAME',
          Text: 'Kongo',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_NORWAY_NAME',
          Text: 'Norway',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_ROME_NAME',
          Text: 'Rome',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_RUSSIA_NAME',
          Text: 'Russia',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_SCYTHIA_NAME',
          Text: 'Scythia',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_SPAIN_NAME',
          Text: 'Spain',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_SUMERIA_NAME',
          Text: 'Sumeria',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_AMSTERDAM_NAME',
          Text: 'Amsterdam',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_BANDAR_BRUNEI_NAME',
          Text: 'Bandar Brunei',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_BRUSSELS_NAME',
          Text: 'Brussels',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_BUENOS_AIRES_NAME',
          Text: 'Buenos Aires',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_CARTHAGE_NAME',
          Text: 'Carthage',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_GENEVA_NAME',
          Text: 'Geneva',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_HATTUSA_NAME',
          Text: 'Hattusa',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_HONG_KONG_NAME',
          Text: 'Hong Kong',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_JAKARTA_NAME',
          Text: 'Jakarta',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_JERUSALEM_NAME',
          Text: 'Jerusalem',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_KABUL_NAME',
          Text: 'Kabul',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_KANDY_NAME',
          Text: 'Kandy',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_KUMASI_NAME',
          Text: 'Kumasi',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_LA_VENTA_NAME',
          Text: 'La Venta',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_LISBON_NAME',
          Text: 'Lisbon',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_MOHENJO_DARO_NAME',
          Text: 'Mohenjo-Daro',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_PRESLAV_NAME',
          Text: 'Preslav',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_NAN_MADOL_NAME',
          Text: 'Nan Madol',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_SEOUL_NAME',
          Text: 'Seoul',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_STOCKHOLM_NAME',
          Text: 'Stockholm',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_TORONTO_NAME',
          Text: 'Toronto',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_VALLETTA_NAME',
          Text: 'Valletta',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_VILNIUS_NAME',
          Text: 'Vilnius',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_YEREVAN_NAME',
          Text: 'Yerevan',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_ZANZIBAR_NAME',
          Text: 'Zanzibar',
        },
        {
          '-Tag': 'LOC_CONTINENT_AFRICA_DESCRIPTION',
          Text: 'Africa',
        },
        {
          '-Tag': 'LOC_CONTINENT_AMASIA_DESCRIPTION',
          Text: 'Amasia',
        },
        {
          '-Tag': 'LOC_CONTINENT_AMERICA_DESCRIPTION',
          Text: 'America',
        },
        {
          '-Tag': 'LOC_CONTINENT_ANTARCTICA_DESCRIPTION',
          Text: 'Antarctica',
        },
        {
          '-Tag': 'LOC_CONTINENT_ARCTICA_DESCRIPTION',
          Text: 'Arctica',
        },
        {
          '-Tag': 'LOC_CONTINENT_ASIA_DESCRIPTION',
          Text: 'Asia',
        },
        {
          '-Tag': 'LOC_CONTINENT_ASIAMERICA_DESCRIPTION',
          Text: 'Asiamerica',
        },
        {
          '-Tag': 'LOC_CONTINENT_ATLANTICA_DESCRIPTION',
          Text: 'Atlantica',
        },
        {
          '-Tag': 'LOC_CONTINENT_ATLANTIS_DESCRIPTION',
          Text: 'Atlantis',
        },
        {
          '-Tag': 'LOC_CONTINENT_AUSTRALIA_DESCRIPTION',
          Text: 'Australia',
        },
        {
          '-Tag': 'LOC_CONTINENT_AVALONIA_DESCRIPTION',
          Text: 'Avalonia',
        },
        {
          '-Tag': 'LOC_CONTINENT_AZANIA_DESCRIPTION',
          Text: 'Azania',
        },
        {
          '-Tag': 'LOC_CONTINENT_BALTICA_DESCRIPTION',
          Text: 'Baltica',
        },
        {
          '-Tag': 'LOC_CONTINENT_CIMMERIA_DESCRIPTION',
          Text: 'Cimmeria',
        },
        {
          '-Tag': 'LOC_CONTINENT_COLUMBIA_DESCRIPTION',
          Text: 'Columbia',
        },
        {
          '-Tag': 'LOC_CONTINENT_CONGO_CRATON_DESCRIPTION',
          Text: 'Congo Craton',
        },
        {
          '-Tag': 'LOC_CONTINENT_EURAMERICA_DESCRIPTION',
          Text: 'Euramerica',
        },
        {
          '-Tag': 'LOC_CONTINENT_EUROPE_DESCRIPTION',
          Text: 'Europe',
        },
        {
          '-Tag': 'LOC_CONTINENT_GONDWANA_DESCRIPTION',
          Text: 'Gondwana',
        },
        {
          '-Tag': 'LOC_CONTINENT_KALAHARIA_DESCRIPTION',
          Text: 'Kalaharia',
        },
        {
          '-Tag': 'LOC_CONTINENT_KAZAKHSTANIA_DESCRIPTION',
          Text: 'Kazakhstania',
        },
        {
          '-Tag': 'LOC_CONTINENT_KERNORLAND_DESCRIPTION',
          Text: 'Kernorland',
        },
        {
          '-Tag': 'LOC_CONTINENT_KUMARI_KANDAM_DESCRIPTION',
          Text: 'Kumari Kandam',
        },
        {
          '-Tag': 'LOC_CONTINENT_LAURASIA_DESCRIPTION',
          Text: 'Laurasia',
        },
        {
          '-Tag': 'LOC_CONTINENT_LAURENTIA_DESCRIPTION',
          Text: 'Laurentia',
        },
        {
          '-Tag': 'LOC_CONTINENT_LEMURIA_DESCRIPTION',
          Text: 'Lemuria',
        },
        {
          '-Tag': 'LOC_CONTINENT_MU_DESCRIPTION',
          Text: 'Mu',
        },
        {
          '-Tag': 'LOC_CONTINENT_NENA_DESCRIPTION',
          Text: 'Nena',
        },
        {
          '-Tag': 'LOC_CONTINENT_NORTH_AMERICA_DESCRIPTION',
          Text: 'North America',
        },
        {
          '-Tag': 'LOC_CONTINENT_NOVOPANGAEA_DESCRIPTION',
          Text: 'Novopangaea',
        },
        {
          '-Tag': 'LOC_CONTINENT_NUNA_DESCRIPTION',
          Text: 'Nuna',
        },
        {
          '-Tag': 'LOC_CONTINENT_OCEANIA_DESCRIPTION',
          Text: 'Oceania',
        },
        {
          '-Tag': 'LOC_CONTINENT_PANGAEA_DESCRIPTION',
          Text: 'Pangaea',
        },
        {
          '-Tag': 'LOC_CONTINENT_PANGAEA_ULTIMA_DESCRIPTION',
          Text: 'Pangaea Ultima',
        },
        {
          '-Tag': 'LOC_CONTINENT_PANNOTIA_DESCRIPTION',
          Text: 'Pannotia',
        },
        {
          '-Tag': 'LOC_CONTINENT_RODINIA_DESCRIPTION',
          Text: 'Rodinia',
        },
        {
          '-Tag': 'LOC_CONTINENT_SIBERIA_DESCRIPTION',
          Text: 'Siberia',
        },
        {
          '-Tag': 'LOC_CONTINENT_SOUTH_AMERICA_DESCRIPTION',
          Text: 'South America',
        },
        {
          '-Tag': 'LOC_CONTINENT_TERRA_AUSTRALIS_DESCRIPTION',
          Text: 'Terra Australis',
        },
        {
          '-Tag': 'LOC_CONTINENT_UR_DESCRIPTION',
          Text: 'Ur',
        },
        {
          '-Tag': 'LOC_CONTINENT_VAALBARA_DESCRIPTION',
          Text: 'Vaalbara',
        },
        {
          '-Tag': 'LOC_CONTINENT_VENDIAN_DESCRIPTION',
          Text: 'Vendian',
        },
        {
          '-Tag': 'LOC_CONTINENT_ZEALANDIA_DESCRIPTION',
          Text: 'Zealandia',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_ALLIED_NAME',
          Text: 'Allied',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_DECLARED_FRIEND_NAME',
          Text: 'Declared Friend',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_FRIENDLY_NAME',
          Text: 'Friendly',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_NEUTRAL_NAME',
          Text: 'Neutral',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_UNFRIENDLY_NAME',
          Text: 'Unfriendly',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_DENOUNCED_NAME',
          Text: 'Denounced',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_WAR_NAME',
          Text: 'At War',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_NO_INFLUENCE_NAME',
          Text: 'Without Influence',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_INFLUENTIAL_NAME',
          Text: 'Some Influence',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_MAX_INFLUENCE_NAME',
          Text: 'Max Influence',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_WAR_WITH_MINOR_NAME',
          Text: 'At war',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_AWARE_NAME',
          Text: 'We know who they are',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_PATRON_NAME',
          Text: 'They have some influence',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_PROTECTOR_NAME',
          Text: 'They are our protectors',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_WAR_WITH_MAJOR_NAME',
          Text: 'At war',
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_MINOR_MINOR_NAME',
          Text: "I've seen you",
        },
        {
          '-Tag': 'LOC_DIPLO_STATE_MINOR_MINOR_WAR_NAME',
          Text: 'Minor civilizations at war',
        },
        {
          '-Tag': 'LOC_DISTRICT_AERODROME_NAME',
          Text: 'Aerodrome',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_DISTRICT_CAMPUS_NAME',
          Text: 'Campus',
        },
        {
          '-Tag': 'LOC_DISTRICT_CITY_CENTER_NAME',
          Text: 'City Center',
        },
        {
          '-Tag': 'LOC_DISTRICT_COMMERCIAL_HUB_NAME',
          Text: 'Commercial Hub',
        },
        {
          '-Tag': 'LOC_DISTRICT_ENCAMPMENT_NAME',
          Text: 'Encampment',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_DISTRICT_ENTERTAINMENT_COMPLEX_NAME',
          Text: 'Entertainment Complex',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_DISTRICT_HARBOR_NAME',
          Text: 'Harbor',
        },
        {
          '-Tag': 'LOC_DISTRICT_HOLY_SITE_NAME',
          Text: 'Holy Site',
        },
        {
          '-Tag': 'LOC_DISTRICT_THEATER_NAME',
          Text: 'Theater Square',
        },
        {
          '-Tag': 'LOC_DISTRICT_INDUSTRIAL_ZONE_NAME',
          Text: 'Industrial Zone',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_DISTRICT_NEIGHBORHOOD_NAME',
          Text: 'Neighborhood',
        },
        {
          '-Tag': 'LOC_DISTRICT_AQUEDUCT_NAME',
          Text: 'Aqueduct',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_DISTRICT_SPACEPORT_NAME',
          Text: 'Spaceport',
        },
        {
          '-Tag': 'LOC_DISTRICT_ACROPOLIS_NAME',
          Text: 'Acropolis',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_DISTRICT_BATH_NAME',
          Text: 'Bath',
        },
        {
          '-Tag': 'LOC_DISTRICT_HANSA_NAME',
          Text: 'Hansa',
        },
        {
          '-Tag': 'LOC_DISTRICT_LAVRA_NAME',
          Text: 'Lavra',
        },
        {
          '-Tag': 'LOC_DISTRICT_MBANZA_NAME',
          Text: 'Mbanza',
        },
        {
          '-Tag': 'LOC_DISTRICT_STREET_CARNIVAL_NAME',
          Text: 'Street Carnival',
        },
        {
          '-Tag': 'LOC_DISTRICT_ROYAL_NAVY_DOCKYARD_NAME',
          Text: 'Royal Navy Dockyard',
        },
        {
          '-Tag': 'LOC_ERA_ANCIENT_NAME',
          Text: 'Ancient Era',
        },
        {
          '-Tag': 'LOC_ERA_CLASSICAL_NAME',
          Text: 'Classical Era',
        },
        {
          '-Tag': 'LOC_ERA_MEDIEVAL_NAME',
          Text: 'Medieval Era',
        },
        {
          '-Tag': 'LOC_ERA_RENAISSANCE_NAME',
          Text: 'Renaissance Era',
        },
        {
          '-Tag': 'LOC_ERA_INDUSTRIAL_NAME',
          Text: 'Industrial Era',
        },
        {
          '-Tag': 'LOC_ERA_MODERN_NAME',
          Text: 'Modern Era',
        },
        {
          '-Tag': 'LOC_ERA_ATOMIC_NAME',
          Text: 'Atomic Era',
        },
        {
          '-Tag': 'LOC_ERA_INFORMATION_NAME',
          Text: 'Information Era',
        },
        {
          '-Tag': 'LOC_GAME_SPEED',
          Text: 'Game Speed',
        },
        {
          '-Tag': 'LOC_GAMESPEED_MARATHON_NAME',
          Text: 'Marathon',
        },
        {
          '-Tag': 'LOC_GAMESPEED_EPIC_NAME',
          Text: 'Epic',
        },
        {
          '-Tag': 'LOC_GAMESPEED_STANDARD_NAME',
          Text: 'Standard',
        },
        {
          '-Tag': 'LOC_GAMESPEED_QUICK_NAME',
          Text: 'Quick',
        },
        {
          '-Tag': 'LOC_GAMESPEED_ONLINE_NAME',
          Text: 'Online',
        },
        {
          '-Tag': 'LOC_MAP_CONTINENTS',
          Text: 'Continents',
        },
        {
          '-Tag': 'LOC_MAP_FRACTAL',
          Text: 'Fractal',
        },
        {
          '-Tag': 'LOC_MAP_INLANDSEA',
          Text: 'Inland Sea',
        },
        {
          '-Tag': 'LOC_MAP_ISLAND_PLATES',
          Text: 'Island Plates',
        },
        {
          '-Tag': 'LOC_MAP_LAKES',
          Text: 'Lakes',
        },
        {
          '-Tag': 'LOC_MAP_MIRROR',
          Text: 'Mirror',
        },
        {
          '-Tag': 'LOC_MAP_PANGAEA',
          Text: 'Pangaea',
        },
        {
          '-Tag': 'LOC_MAP_SEVEN_SEAS',
          Text: 'Seven Seas',
        },
        {
          '-Tag': 'LOC_MAP_SMALL_CONTINENTS',
          Text: 'Small Continents',
        },
        {
          '-Tag': 'LOC_MAP_SHUFFLE',
          Text: 'Shuffle',
        },
        {
          '-Tag': 'LOC_MAP_TERRA',
          Text: 'Terra',
        },
        {
          '-Tag': 'LOC_MAP_SIZE',
          Text: 'Map Size',
        },
        {
          '-Tag': 'LOC_MAPSIZE_DUEL_NAME',
          Text: 'Duel',
        },
        {
          '-Tag': 'LOC_MAPSIZE_TINY_NAME',
          Text: 'Tiny',
        },
        {
          '-Tag': 'LOC_MAPSIZE_SMALL_NAME',
          Text: 'Small',
        },
        {
          '-Tag': 'LOC_MAPSIZE_STANDARD_NAME',
          Text: 'Standard',
        },
        {
          '-Tag': 'LOC_MAPSIZE_LARGE_NAME',
          Text: 'Large',
        },
        {
          '-Tag': 'LOC_MAPSIZE_HUGE_NAME',
          Text: 'Huge',
        },
        {
          '-Tag': 'LOC_FEATURE_FLOODPLAINS_NAME',
          Text: 'Floodplains',
        },
        {
          '-Tag': 'LOC_FEATURE_FOREST_NAME',
          Text: 'Woods',
        },
        {
          '-Tag': 'LOC_FEATURE_ICE_NAME',
          Text: 'Ice',
        },
        {
          '-Tag': 'LOC_FEATURE_JUNGLE_NAME',
          Text: 'Rainforest',
        },
        {
          '-Tag': 'LOC_FEATURE_MARSH_NAME',
          Text: 'Marsh',
        },
        {
          '-Tag': 'LOC_FEATURE_OASIS_NAME',
          Text: 'Oasis',
        },
        {
          '-Tag': 'LOC_FEATURE_BARRIER_REEF_NAME',
          Text: 'Great Barrier Reef',
        },
        {
          '-Tag': 'LOC_FEATURE_CLIFFS_DOVER_NAME',
          Text: 'Cliffs of Dover',
        },
        {
          '-Tag': 'LOC_FEATURE_CRATER_LAKE_NAME',
          Text: 'Crater Lake',
        },
        {
          '-Tag': 'LOC_FEATURE_DEAD_SEA_NAME',
          Text: 'Dead Sea',
        },
        {
          '-Tag': 'LOC_FEATURE_EVEREST_NAME',
          Text: 'Mount Everest',
        },
        {
          '-Tag': 'LOC_FEATURE_GALAPAGOS_NAME',
          Text: 'Galápagos Islands',
        },
        {
          '-Tag': 'LOC_FEATURE_GRAND_CANYON',
          Text: 'Grand Canyon',
        },
        {
          '-Tag': 'LOC_FEATURE_KILIMANJARO_NAME',
          Text: 'Mount Kilimanjaro',
        },
        {
          '-Tag': 'LOC_FEATURE_PANTANAL_NAME',
          Text: 'Pantanal',
        },
        {
          '-Tag': 'LOC_FEATURE_PIOPIOTAHI_NAME',
          Text: 'Piopiotahi',
        },
        {
          '-Tag': 'LOC_FEATURE_TORRES_DEL_PAINE_NAME',
          Text: 'Torres del Paine',
        },
        {
          '-Tag': 'LOC_FEATURE_TSINGY_NAME',
          Text: 'Tsingy de Bemaraha',
        },
        {
          '-Tag': 'LOC_FEATURE_YOSEMITE_NAME',
          Text: 'Yosemite',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_BOUDICA_NAME',
          Text: 'Boudica',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HANNIBAL_BARCA_NAME',
          Text: 'Hannibal Barca',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SUN_TZU_NAME',
          Text: 'Sun Tzu',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_AETHELFLAED_NAME',
          Text: 'Æthelflæd',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EL_CID_NAME',
          Text: 'El Cid',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GENGHIS_KHAN_NAME',
          Text: 'Genghis Khan',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GUSTAVUS_ADOLPHUS_NAME',
          Text: 'Gustavus Adolphus',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOAN_OF_ARC_NAME',
          Text: "Jeanne d'Arc",
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ANA_NZINGA_NAME',
          Text: 'Ana Nzinga',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_NAPOLEON_BONAPARTE_NAME',
          Text: 'Napoleon Bonaparte',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_RANI_LAKSHMIBAI_NAME',
          Text: 'Rani Lakshmibai',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SIMON_BOLIVAR_NAME',
          Text: 'Simón Bolívar',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHN_MONASH_NAME',
          Text: 'John Monash',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARINA_RASKOVA_NAME',
          Text: 'Marina Raskova',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SAMORI_TURE_NAME',
          Text: 'Samori Touré',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_DWIGHT_EISENHOWER_NAME',
          Text: 'Dwight Eisenhower',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GEORGY_ZHUKOV_NAME',
          Text: 'Georgy Zhukov',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_DOUGLAS_MACARTHUR_NAME',
          Text: 'Douglas MacArthur',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SUDIRMAN_NAME',
          Text: 'Sudirman',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_AHMAD_SHAH_MASSOUD_NAME',
          Text: 'Ahmad Shah Massoud',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_VIJAYA_WIMALARATNE_NAME',
          Text: 'Vijaya Wimalaratne',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ARTEMISIA_NAME',
          Text: 'Artemisia',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GAIUS_DUILIUS_NAME',
          Text: 'Gaius Duilius',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_THEMISTOCLES_NAME',
          Text: 'Themistocles',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LEIF_ERIKSON_NAME',
          Text: 'Leif Erikson',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_RAJENDRA_CHOLA_NAME',
          Text: 'Rajendra Chola',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ZHENG_HE_NAME',
          Text: 'Zheng He',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_FRANCIS_DRAKE_NAME',
          Text: 'Francis Drake',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SANTA_CRUZ_NAME',
          Text: 'Santa Cruz',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_YI_SUN_SIN_NAME',
          Text: 'Yi Sun-sin',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CHING_SHIH_NAME',
          Text: 'Ching Shih',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HORATIO_NELSON_NAME',
          Text: 'Horatio Nelson',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LASKARINA_BOUBOULINA_NAME',
          Text: 'Laskarina Bouboulina',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_FRANZ_VON_HIPPER_NAME',
          Text: 'Franz von Hipper',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOAQUIM_MARQUES_LISBOA_NAME',
          Text: 'Joaquim Marques Lisboa',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_TOGO_HEIHACHIRO_NAME',
          Text: 'Togo Heihachiro',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CHESTER_NIMITZ_NAME',
          Text: 'Chester Nimitz',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GRACE_HOPPER_NAME',
          Text: 'Grace Hopper',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SERGEY_GORSHKOV_NAME',
          Text: 'Sergei Gorshkov',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CLANCY_FERNANDO_NAME',
          Text: 'Clancy Fernando',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_BI_SHENG_NAME',
          Text: 'Bi Sheng',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ISIDORE_OF_MILETUS_NAME',
          Text: 'Isidore of Miletus',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JAMES_OF_ST_GEORGE_NAME',
          Text: 'James of St. George',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_FILIPPO_BRUNELLESCHI_NAME',
          Text: 'Filippo Brunelleschi',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LEONARDO_DA_VINCI_NAME',
          Text: 'Leonardo da Vinci',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MIMAR_SINAN_NAME',
          Text: 'Mimar Sinan',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ADA_LOVELACE_NAME',
          Text: 'Ada Lovelace',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GUSTAVE_EIFFEL_NAME',
          Text: 'Gustave Eiffel',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JAMES_WATT_NAME',
          Text: 'James Watt',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ALVAR_AALTO_NAME',
          Text: 'Alvar Aalto',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_NIKOLA_TESLA_NAME',
          Text: 'Nikola Tesla',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ROBERT_GODDARD_NAME',
          Text: 'Robert Goddard',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHN_A_ROEBLING_NAME',
          Text: 'John Roebling',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JANE_DREW_NAME',
          Text: 'Jane Drew',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SERGEI_KOROLEV_NAME',
          Text: 'Sergei Korolev',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CHARLES_CORREA_NAME',
          Text: 'Charles Correa',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOSEPH_PAXTON_NAME',
          Text: 'Joseph Paxton',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_WERNHER_VON_BRAUN_NAME',
          Text: 'Wernher von Braun',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_COLAEUS_NAME',
          Text: 'Colaeus',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARCUS_LICINIUS_CRASSUS_NAME',
          Text: 'Marcus Licinius Crassus',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ZHANG_QIAN_NAME',
          Text: 'Zhang Qian',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARCO_POLO_NAME',
          Text: 'Marco Polo',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_PIERO_DE_BARDI_NAME',
          Text: "Piero de' Bardi",
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_IRENE_OF_ATHENS_NAME',
          Text: 'Irene of Athens',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GIOVANNI_DE_MEDICI_NAME',
          Text: "Giovanni de' Medici",
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JAKOB_FUGGER_NAME',
          Text: 'Jakob Fugger',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_RAJA_TODAR_MAL_NAME',
          Text: 'Raja Todar Mal',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ADAM_SMITH_NAME',
          Text: 'Adam Smith',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHN_SPILSBURY',
          Text: 'John Spilsbury',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHN_JACOB_ASTOR_NAME',
          Text: 'John Jacob Astor',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LEVI_STRAUSS_NAME',
          Text: 'Levi Strauss',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHN_ROCKEFELLER_NAME',
          Text: 'John Rockefeller',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SARAH_BREEDLOVE_NAME',
          Text: 'Sarah Breedlove',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HELENA_RUBINSTEIN_NAME',
          Text: 'Helena Rubinstein',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MELITTA_BENTZ',
          Text: 'Melitta Bentz',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARY_KATHERINE_GODDARD_NAME',
          Text: 'Mary Katherine Goddard',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ESTEE_LAUDER',
          Text: 'Estée Lauder',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MASARU_IBUKA_NAME',
          Text: 'Masaru Ibuka',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JAMSETJI_TATA_NAME',
          Text: 'Jamsetji Tata',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHN_THE_BAPTIST_NAME',
          Text: 'John the Baptist',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CONFUCIUS_NAME',
          Text: 'Confucius',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ZOROASTER_NAME',
          Text: 'Zoroaster',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SIDDHARTHA_GAUTAMA_NAME',
          Text: 'Siddhartha Gautama',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LAOZI_NAME',
          Text: 'Laozi',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SIMON_PETER_NAME',
          Text: 'Simon Peter',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_BODHIDHARMA_NAME',
          Text: 'Bodhidharma',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ADI_SHANKARA_NAME',
          Text: 'Adi Shankara',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SONGTSAN_GAMPO_NAME',
          Text: 'Songtsan Gampo',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_O_NO_YASUMARO_NAME',
          Text: 'O no Yasumaro',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_IRENAEUS_NAME',
          Text: 'Irenaeus',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARTIN_LUTHER_NAME',
          Text: 'Martin Luther',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_FRANCIS_OF_ASSISI_NAME',
          Text: 'Francis of Assisi',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MADHVA_ACHARYA_NAME',
          Text: 'Madhva Acharya',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_THOMAS_AQUINAS_NAME',
          Text: 'Thomas Aquinas',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HAJI_HUUD_NAME',
          Text: 'Haji Huud',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ARYABHATA_NAME',
          Text: 'Aryabhata',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EUCLID_NAME',
          Text: 'Euclid',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HYPATIA_NAME',
          Text: 'Hypatia',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ABU_AL_QASIM_AL_ZAHRAWI_NAME',
          Text: 'Abu al-Qasim al-Zahrawi',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HILDEGARD_OF_BINGEN_NAME',
          Text: 'Hildegard of Bingen',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_OMAR_KHAYYAM_NAME',
          Text: 'Omar Khayyam',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GALILEO_GALILEI_NAME',
          Text: 'Galileo Galilei',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ISAAC_NEWTON_NAME',
          Text: 'Isaac Newton',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EMILIE_DU_CHATELET_NAME',
          Text: 'Emilie du Chatelet',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ALFRED_NOBEL_NAME',
          Text: 'Alfred Nobel',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CHARLES_DARWIN_NAME',
          Text: 'Charles Darwin',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_DMITRI_MENDELEEV_NAME',
          Text: 'Dmitri Mendeleev',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ALAN_TURING_NAME',
          Text: 'Alan Turing',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ALBERT_EINSTEIN_NAME',
          Text: 'Albert Einstein',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JAMES_YOUNG_NAME',
          Text: 'James Young',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JANAKI_AMMAL_NAME',
          Text: 'Janaki Ammal',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARY_LEAKEY_NAME',
          Text: 'Mary Leakey',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ERWIN_SCHRODINGER_NAME',
          Text: 'Erwin Schrödinger',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ABDUS_SALAM_NAME',
          Text: 'Abdus Salam',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CARL_SAGAN_NAME',
          Text: 'Carl Sagan',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_STEPHANIE_KWOLEK_NAME',
          Text: 'Stephanie Kwolek',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_BHASA_NAME',
          Text: 'Bhasa',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HOMER_NAME',
          Text: 'Homer',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_OVID_NAME',
          Text: 'Ovid',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_QU_YUAN_NAME',
          Text: 'Qu Yuan',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GEOFFREY_CHAUCER_NAME',
          Text: 'Geoffrey Chaucer',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LI_BAI_NAME',
          Text: 'Li Bai',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MURASAKI_SHIKIBU_NAME',
          Text: 'Murasaki Shikibu',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARGARET_CAVENDISH_NAME',
          Text: 'Margaret Cavendish',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MIGUEL_DE_CERVANTES_NAME',
          Text: 'Miguel de Cervantes',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_NICCOLO_MACHIAVELLI_NAME',
          Text: 'Niccolò Machiavelli',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_WILLIAM_SHAKESPEARE_NAME',
          Text: 'William Shakespeare',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARIE_CATHERINE_D_AULNOY_NAME',
          Text: "Marie-Catherine d'Aulnoy",
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ALEXANDER_PUSHKIN_NAME',
          Text: 'Alexander Pushkin',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EDGAR_ALLEN_POE_NAME',
          Text: 'Edgar Allan Poe',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JANE_AUSTEN_NAME',
          Text: 'Jane Austen',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHANN_WOLFGANG_VON_GOETHE_NAME',
          Text: 'Johann Wolfgang von Goethe',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARY_SHELLEY_NAME',
          Text: 'Mary Shelley',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ABDUL_MUIS_NAME',
          Text: 'Abdul Muis',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JAMES_JOYCE_NAME',
          Text: 'James Joyce',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EMILY_DICKINSON_NAME',
          Text: 'Emily Dickinson',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LEO_TOLSTOY_NAME',
          Text: 'Leo Tolstoy',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARK_TWAIN_NAME',
          Text: 'Mark Twain',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_F_SCOTT_FITZGERALD_NAME',
          Text: 'F. Scott Fitzgerald',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HG_WELLS_NAME',
          Text: 'H.G. Wells',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_RABINDRANATH_TAGORE_NAME',
          Text: 'Rabindranath Tagore',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_KAREL_CAPEK_NAME',
          Text: 'Karel Capek',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ANDREY_RUBLEV_NAME',
          Text: 'Andrei Rublev',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_HIERONYMUS_BOSCH_NAME',
          Text: 'Hieronymus Bosch',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_DONATELLO_NAME',
          Text: 'Donatello',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MICHELANGELO_NAME',
          Text: 'Michelangelo',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_QIU_YING_NAME',
          Text: 'Qiu Ying',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_TITIAN_NAME',
          Text: 'Titian',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ANGELICA_KAUFFMAN_NAME',
          Text: 'Angelica Kauffman',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EL_GRECO_NAME',
          Text: 'El Greco',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_REMBRANDT_VAN_RIJN_NAME',
          Text: 'Rembrandt van Rijn',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_SOFONISBA_ANGUISSOLA_NAME',
          Text: 'Sofonisba Anguissola',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JANG_SEUNG_EOP_NAME',
          Text: 'Jang Seung-eop',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_KATSUSHIKA_HOKUSAI_NAME',
          Text: 'Katsushika Hokusai',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_VINCENT_VAN_GOGH_NAME',
          Text: 'Vincent van Gogh',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_EDMONIA_LEWIS_NAME',
          Text: 'Edmonia Lewis',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_BORIS_ORLOVSKY_NAME',
          Text: 'Boris Orlovsky',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GUSTAV_KLIMT_NAME',
          Text: 'Gustav Klimt',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARIE_ANNE_COLLOT_NAME',
          Text: 'Marie-Anne Collot',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CLAUDE_MONET_NAME',
          Text: 'Claude Monet',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_AMRITA_SHER_GIL_NAME',
          Text: 'Amrita Sher-Gil',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MARY_CASSATT_NAME',
          Text: 'Mary Cassatt',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ANTONIO_VIVALDI_NAME',
          Text: 'Antonio Vivaldi',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JOHANN_SEBASTIAN_BACH_NAME',
          Text: 'Johann Sebastian Bach',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LUDWIG_VAN_BEETHOVEN_NAME',
          Text: 'Ludwig van Beethoven',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_WOLFGANG_AMADEUS_MOZART_NAME',
          Text: 'Wolfgang Amadeus Mozart',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_YATSUHASHI_KENGYO_NAME',
          Text: 'Yatsuhashi Kengyo',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ANTONIO_CARLOS_GOMEZ_NAME',
          Text: 'Antônio Carlos Gomes',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_FRANZ_LISZT_NAME',
          Text: 'Franz Liszt',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_FREDERIC_CHOPIN_NAME',
          Text: 'Frederic Chopin',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_PETER_ILYICH_TCHAIKOVSKY_NAME',
          Text: 'Peter Ilyich Tchaikovsky',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LIU_TIANHUA_NAME',
          Text: 'Liu Tianhua',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_ANTONIN_DVORAK_NAME',
          Text: 'Antonin Dvorak',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_CLARA_SCHUMANN_NAME',
          Text: 'Clara Schumann',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_JUVENTINO_ROSAS_NAME',
          Text: 'Juventino Rosas',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_LILIUOKALANI_NAME',
          Text: "Lili'uokalani",
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_GAUHAR_JAAN_NAME',
          Text: 'Gauhar Jaan',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_INDIVIDUAL_MYKOLA_LEONTOVYCH_NAME',
          Text: 'Mykola Leontovych',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_SCULPTURE_NAME',
          Text: 'Sculpture',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_PORTRAIT_NAME',
          Text: 'Portrait',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_LANDSCAPE_NAME',
          Text: 'Landscape',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_RELIGIOUS_NAME',
          Text: 'Religious',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_ARTIFACT_NAME',
          Text: 'Artifact',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_WRITING_NAME',
          Text: 'Writing',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_MUSIC_NAME',
          Text: 'Music',
        },
        {
          '-Tag': 'LOC_GREAT_WORK_OBJECT_RELIC_NAME',
          Text: 'Relic',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_ANARCHY_NAME',
          Text: 'Anarchy',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_CHIEFDOM_NAME',
          Text: 'Chiefdom',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_AUTOCRACY_NAME',
          Text: 'Autocracy',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_OLIGARCHY_NAME',
          Text: 'Oligarchy',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_CLASSICAL_REPUBLIC_NAME',
          Text: 'Classical Republic',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_MONARCHY_NAME',
          Text: 'Monarchy',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_THEOCRACY_NAME',
          Text: 'Theocracy',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_MERCHANT_REPUBLIC_NAME',
          Text: 'Merchant Republic',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_FASCISM_NAME',
          Text: 'Fascism',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_COMMUNISM_NAME',
          Text: 'Communism',
        },
        {
          '-Tag': 'LOC_GOVERNMENT_DEMOCRACY_NAME',
          Text: 'Democracy',
        },
        {
          '-Tag': 'LOC_HAPPINESS_REVOLT_NAME',
          Text: 'Revolt',
        },
        {
          '-Tag': 'LOC_HAPPINESS_UNREST_NAME',
          Text: 'Unrest',
        },
        {
          '-Tag': 'LOC_HAPPINESS_UNHAPPY_NAME',
          Text: 'Unhappy',
        },
        {
          '-Tag': 'LOC_HAPPINESS_DISPLEASED_NAME',
          Text: 'Displeased',
        },
        {
          '-Tag': 'LOC_HAPPINESS_CONTENT_NAME',
          Text: 'Content',
        },
        {
          '-Tag': 'LOC_HAPPINESS_HAPPY_NAME',
          Text: 'Happy',
        },
        {
          '-Tag': 'LOC_HAPPINESS_ECSTATIC_NAME',
          Text: 'Ecstatic',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_BARBARIAN_CAMP_NAME',
          Text: 'Barbarian Outpost',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_GOODY_HUT_NAME',
          Text: 'Tribal Village',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_FARM_NAME',
          Text: 'Farm',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_MINE_NAME',
          Text: 'Mine',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_QUARRY_NAME',
          Text: 'Quarry',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_FISHING_BOATS_NAME',
          Text: 'Fishing Boats',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_PASTURE_NAME',
          Text: 'Pasture',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_PLANTATION_NAME',
          Text: 'Plantation',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_CAMP_NAME',
          Text: 'Camp',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_LUMBER_MILL_NAME',
          Text: 'Lumber Mill',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_OIL_WELL_NAME',
          Text: 'Oil Well',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_OFFSHORE_OIL_RIG_NAME',
          Text: 'Offshore Oil Rig',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_BEACH_RESORT_NAME',
          Text: 'Seaside Resort',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_FORT_NAME',
          Text: 'Fort',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_AIRSTRIP_NAME',
          Text: 'Airstrip',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_CHATEAU_NAME',
          Text: 'Château',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_COLOSSAL_HEAD_NAME',
          Text: 'Colossal Head',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_GREAT_WALL_NAME',
          Text: 'Great Wall',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_KURGAN_NAME',
          Text: 'Kurgan',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_MISSION_NAME',
          Text: 'Mission',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_ROMAN_FORT_NAME',
          Text: 'Roman Fort',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_SPHINX_NAME',
          Text: 'Sphinx',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_STEPWELL_NAME',
          Text: 'Stepwell',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_ZIGGURAT_NAME',
          Text: 'Ziggurat',
        },
        {
          '-Tag': 'LOC_IMPROVEMENT_MISSILE_SILO',
          Text: 'Missile Silo',
        },
        {
          '-Tag': 'LOC_POLICY_AESTHETICS_NAME',
          Text: 'Aesthetics',
        },
        {
          '-Tag': 'LOC_POLICY_AGOGE_NAME',
          Text: 'Agoge',
        },
        {
          '-Tag': 'LOC_POLICY_ARSENAL_OF_DEMOCRACY_NAME',
          Text: 'Arsenal of Democracy',
        },
        {
          '-Tag': 'LOC_POLICY_BASTIONS_NAME',
          Text: 'Bastions',
        },
        {
          '-Tag': 'LOC_POLICY_CARAVANSARIES_NAME',
          Text: 'Caravansaries',
        },
        {
          '-Tag': 'LOC_POLICY_CHARISMATIC_LEADER_NAME',
          Text: 'Charismatic Leader',
        },
        {
          '-Tag': 'LOC_POLICY_CHIVALRY_NAME',
          Text: 'Chivalry',
        },
        {
          '-Tag': 'LOC_POLICY_COLLECTIVE_ACTIVISM_NAME',
          Text: 'Collective Activism',
        },
        {
          '-Tag': 'LOC_POLICY_COLLECTIVIZATION_NAME',
          Text: 'Collectivization',
        },
        {
          '-Tag': 'LOC_POLICY_COLONIAL_OFFICES_NAME',
          Text: 'Colonial Offices',
        },
        {
          '-Tag': 'LOC_POLICY_COLONIAL_TAXES_NAME',
          Text: 'Colonial Taxes',
        },
        {
          '-Tag': 'LOC_POLICY_COLONIZATION_NAME',
          Text: 'Colonization',
        },
        {
          '-Tag': 'LOC_POLICY_CONSCRIPTION_NAME',
          Text: 'Conscription',
        },
        {
          '-Tag': 'LOC_POLICY_CONTAINMENT_NAME',
          Text: 'Containment',
        },
        {
          '-Tag': 'LOC_POLICY_CORVEE_NAME',
          Text: 'Corvée',
        },
        {
          '-Tag': 'LOC_POLICY_CRAFTSMEN_NAME',
          Text: 'Craftsmen',
        },
        {
          '-Tag': 'LOC_POLICY_CRYPTOGRAPHY_NAME',
          Text: 'Cryptography',
        },
        {
          '-Tag': 'LOC_POLICY_DEFENSE_OF_MOTHERLAND_NAME',
          Text: 'Defense of the Motherland',
        },
        {
          '-Tag': 'LOC_POLICY_DIPLOMATIC_LEAGUE_NAME',
          Text: 'Diplomatic League',
        },
        {
          '-Tag': 'LOC_POLICY_DISCIPLINE_NAME',
          Text: 'Discipline',
        },
        {
          '-Tag': 'LOC_POLICY_ECOMMERCE_NAME',
          Text: 'Ecommerce',
        },
        {
          '-Tag': 'LOC_POLICY_ECONOMIC_UNION_NAME',
          Text: 'Economic Union',
        },
        {
          '-Tag': 'LOC_POLICY_EXPROPRIATION_NAME',
          Text: 'Expropriation',
        },
        {
          '-Tag': 'LOC_POLICY_FEUDAL_CONTRACT_NAME',
          Text: 'Feudal Contract',
        },
        {
          '-Tag': 'LOC_POLICY_FINEST_HOUR_NAME',
          Text: 'Their Finest Hour',
        },
        {
          '-Tag': 'LOC_POLICY_FIVE_YEAR_PLAN_NAME',
          Text: 'Five-Year Plan',
        },
        {
          '-Tag': 'LOC_POLICY_FREE_MARKET_NAME',
          Text: 'Free Market',
        },
        {
          '-Tag': 'LOC_POLICY_FRESCOES_NAME',
          Text: 'Frescoes',
        },
        {
          '-Tag': 'LOC_POLICY_GOD_KING_NAME',
          Text: 'God King',
        },
        {
          '-Tag': 'LOC_POLICY_GOTHIC_ARCHITECTURE_NAME',
          Text: 'Gothic Architecture',
        },
        {
          '-Tag': 'LOC_POLICY_GRAND_OPERA_NAME',
          Text: 'Grand Opera',
        },
        {
          '-Tag': 'LOC_POLICY_GRANDE_ARMEE_NAME',
          Text: 'Grande Armée',
        },
        {
          '-Tag': 'LOC_POLICY_GUNBOAT_DIPLOMACY_NAME',
          Text: 'Gunboat Diplomacy',
        },
        {
          '-Tag': 'LOC_POLICY_HERITAGE_TOURISM_NAME',
          Text: 'Heritage Tourism',
        },
        {
          '-Tag': 'LOC_POLICY_ILKUM_NAME',
          Text: 'Ilkum',
        },
        {
          '-Tag': 'LOC_POLICY_INSPIRATION_NAME',
          Text: 'Inspiration',
        },
        {
          '-Tag': 'LOC_POLICY_INSULAE_NAME',
          Text: 'Insulae',
        },
        {
          '-Tag': 'LOC_POLICY_INTEGRATED_SPACE_CELL_NAME',
          Text: 'Integrated Space Cell',
        },
        {
          '-Tag': 'LOC_POLICY_INTERNATIONAL_SPACE_AGENCY_NAME',
          Text: 'International Space Agency',
        },
        {
          '-Tag': 'LOC_POLICY_INTERNATIONAL_WATERS_NAME',
          Text: 'International Waters',
        },
        {
          '-Tag': 'LOC_POLICY_INVENTION_NAME',
          Text: 'Invention',
        },
        {
          '-Tag': 'LOC_POLICY_LAISSEZ_FAIRE_NAME',
          Text: 'Laissez-Faire',
        },
        {
          '-Tag': 'LOC_POLICY_LAND_SURVEYORS_NAME',
          Text: 'Land Surveyors',
        },
        {
          '-Tag': 'LOC_POLICY_LEVEE_EN_MASSE_NAME',
          Text: 'Levée en Masse',
        },
        {
          '-Tag': 'LOC_POLICY_LIBERALISM_NAME',
          Text: 'Liberalism',
        },
        {
          '-Tag': 'LOC_POLICY_LIGHTNING_WARFARE_NAME',
          Text: 'Lightning Warfare',
        },
        {
          '-Tag': 'LOC_POLICY_LIMES_NAME',
          Text: 'Limes',
        },
        {
          '-Tag': 'LOC_POLICY_LITERARY_TRADITION_NAME',
          Text: 'Literary Tradition',
        },
        {
          '-Tag': 'LOC_POLICY_LOGISTICS_NAME',
          Text: 'Logistics',
        },
        {
          '-Tag': 'LOC_POLICY_MACHIAVELLIANISM_NAME',
          Text: 'Machiavellianism',
        },
        {
          '-Tag': 'LOC_POLICY_MANEUVER_NAME',
          Text: 'Maneuver',
        },
        {
          '-Tag': 'LOC_POLICY_MARITIME_INDUSTRIES_NAME',
          Text: 'Maritime Industries',
        },
        {
          '-Tag': 'LOC_POLICY_MARKET_ECONOMY_NAME',
          Text: 'Market Economy',
        },
        {
          '-Tag': 'LOC_POLICY_MARTIAL_LAW_NAME',
          Text: 'Martial Law',
        },
        {
          '-Tag': 'LOC_POLICY_MEDINA_QUARTER_NAME',
          Text: 'Medina Quarter',
        },
        {
          '-Tag': 'LOC_POLICY_MERCHANT_CONFEDERATION_NAME',
          Text: 'Merchant Confederation',
        },
        {
          '-Tag': 'LOC_POLICY_MERITOCRACY_NAME',
          Text: 'Meritocracy',
        },
        {
          '-Tag': 'LOC_POLICY_MILITARY_FIRST_NAME',
          Text: 'Military First',
        },
        {
          '-Tag': 'LOC_POLICY_MILITARY_ORGANIZATION_NAME',
          Text: 'Military Organization',
        },
        {
          '-Tag': 'LOC_POLICY_MILITARY_RESEARCH_NAME',
          Text: 'Military Research',
        },
        {
          '-Tag': 'LOC_POLICY_NATIONAL_IDENTITY_NAME',
          Text: 'National Identity',
        },
        {
          '-Tag': 'LOC_POLICY_NATIVE_CONQUEST_NAME',
          Text: 'Native Conquest',
        },
        {
          '-Tag': 'LOC_POLICY_NATURAL_PHILOSOPHY_NAME',
          Text: 'Natural Philosophy',
        },
        {
          '-Tag': 'LOC_POLICY_NAVAL_INFRASTRUCTURE_NAME',
          Text: 'Naval Infrastructure',
        },
        {
          '-Tag': 'LOC_POLICY_NAVIGATION_NAME',
          Text: 'Navigation',
        },
        {
          '-Tag': 'LOC_POLICY_NEW_DEAL_NAME',
          Text: 'New Deal',
        },
        {
          '-Tag': 'LOC_POLICY_NOBEL_PRIZE_NAME',
          Text: 'Nobel Prize',
        },
        {
          '-Tag': 'LOC_POLICY_NUCLEAR_ESPIONAGE_NAME',
          Text: 'Nuclear Espionage',
        },
        {
          '-Tag': 'LOC_POLICY_ONLINE_COMMUNITIES_NAME',
          Text: 'Online Communities',
        },
        {
          '-Tag': 'LOC_POLICY_PATRIOTIC_WAR_NAME',
          Text: 'Patriotic War',
        },
        {
          '-Tag': 'LOC_POLICY_POLICE_STATE_NAME',
          Text: 'Police State',
        },
        {
          '-Tag': 'LOC_POLICY_PRESS_GANGS_NAME',
          Text: 'Press Gangs',
        },
        {
          '-Tag': 'LOC_POLICY_PROFESSIONAL_ARMY_NAME',
          Text: 'Professional Army',
        },
        {
          '-Tag': 'LOC_POLICY_PROPAGANDA_NAME',
          Text: 'Propaganda',
        },
        {
          '-Tag': 'LOC_POLICY_PUBLIC_TRANSPORT_NAME',
          Text: 'Public Transport',
        },
        {
          '-Tag': 'LOC_POLICY_PUBLIC_WORKS_NAME',
          Text: 'Public Works',
        },
        {
          '-Tag': 'LOC_POLICY_RAJ_NAME',
          Text: 'Raj',
        },
        {
          '-Tag': 'LOC_POLICY_RAID_NAME',
          Text: 'Raid',
        },
        {
          '-Tag': 'LOC_POLICY_RATIONALISM_NAME',
          Text: 'Rationalism',
        },
        {
          '-Tag': 'LOC_POLICY_RELIGIOUS_ORDERS_NAME',
          Text: 'Religious Orders',
        },
        {
          '-Tag': 'LOC_POLICY_RESOURCE_MANAGEMENT_NAME',
          Text: 'Resource Management',
        },
        {
          '-Tag': 'LOC_POLICY_RETAINERS_NAME',
          Text: 'Retainers',
        },
        {
          '-Tag': 'LOC_POLICY_REVELATION_NAME',
          Text: 'Revelation',
        },
        {
          '-Tag': 'LOC_POLICY_SACK_NAME',
          Text: 'Sack',
        },
        {
          '-Tag': 'LOC_POLICY_SATELLITE_BROADCASTS_NAME',
          Text: 'Satellite Broadcasts',
        },
        {
          '-Tag': 'LOC_POLICY_SCRIPTURE_NAME',
          Text: 'Scripture',
        },
        {
          '-Tag': 'LOC_POLICY_SERFDOM_NAME',
          Text: 'Serfdom',
        },
        {
          '-Tag': 'LOC_POLICY_SIMULTANEUM_NAME',
          Text: 'Simultaneum',
        },
        {
          '-Tag': 'LOC_POLICY_SKYSCRAPERS_NAME',
          Text: 'Skyscrapers',
        },
        {
          '-Tag': 'LOC_POLICY_SPORTS_MEDIA_NAME',
          Text: 'Sports Media',
        },
        {
          '-Tag': 'LOC_POLICY_STRATEGIC_AIR_FORCE_NAME',
          Text: 'Strategic Air Force',
        },
        {
          '-Tag': 'LOC_POLICY_STRATEGOS_NAME',
          Text: 'Strategos',
        },
        {
          '-Tag': 'LOC_POLICY_SURVEY_NAME',
          Text: 'Survey',
        },
        {
          '-Tag': 'LOC_POLICY_SYMPHONIES_NAME',
          Text: 'Symphonies',
        },
        {
          '-Tag': 'LOC_POLICY_THIRD_ALTERNATIVE_NAME',
          Text: 'Third Alternative',
        },
        {
          '-Tag': 'LOC_POLICY_TOWN_CHARTERS_NAME',
          Text: 'Town Charters',
        },
        {
          '-Tag': 'LOC_POLICY_TRADE_CONFEDERATION_NAME',
          Text: 'Trade Confederation',
        },
        {
          '-Tag': 'LOC_POLICY_TRAVELING_MERCHANTS_NAME',
          Text: 'Traveling Merchants',
        },
        {
          '-Tag': 'LOC_POLICY_TRIANGULAR_TRADE_NAME',
          Text: 'Triangular Trade',
        },
        {
          '-Tag': 'LOC_POLICY_TOTAL_WAR_NAME',
          Text: 'Total War',
        },
        {
          '-Tag': 'LOC_POLICY_URBAN_PLANNING_NAME',
          Text: 'Urban Planning',
        },
        {
          '-Tag': 'LOC_POLICY_VETERANCY_NAME',
          Text: 'Veterancy',
        },
        {
          '-Tag': 'LOC_POLICY_WARS_OF_RELIGION_NAME',
          Text: 'Wars of Religion',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_ENCAMPMENT_NAME',
          Text: 'Encampment Training',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_ENCAMPMENT_SHORT_NAME',
          Text: 'Encampment Training',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_HARBOR_NAME',
          Text: 'Harbor Shipping',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_HARBOR_SHORT_NAME',
          Text: 'Harbor Shipping',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_INDUSTRIAL_ZONE_NAME',
          Text: 'Industrial Zone Logistics',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_INDUSTRIAL_ZONE_SHORT_NAME',
          Text: 'Industrial Zone Logistics',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_COMMERCIAL_HUB_NAME',
          Text: 'Commercial Hub Investment',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_COMMERCIAL_HUB_SHORT_NAME',
          Text: 'Commercial Hub Investment',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_HOLY_SITE_NAME',
          Text: 'Holy Site Prayers',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_HOLY_SITE_SHORT_NAME',
          Text: 'Holy Site Prayers',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_CAMPUS_NAME',
          Text: 'Campus Research Grants',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_CAMPUS_SHORT_NAME',
          Text: 'Campus Research Grants',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_THEATER_NAME',
          Text: 'Theater Square Festival',
        },
        {
          '-Tag': 'LOC_PROJECT_ENHANCE_DISTRICT_THEATER_SHORT_NAME',
          Text: 'Theater Square Festival',
        },
        {
          '-Tag': 'LOC_PROJECT_CARNIVAL_NAME',
          Text: 'Carnival',
        },
        {
          '-Tag': 'LOC_PROJECT_CARNIVAL_SHORT_NAME',
          Text: 'Carnival',
        },
        {
          '-Tag': 'LOC_PROJECT_REPAIR_OUTER_DEFENSES_NAME',
          Text: 'Repair Outer Defenses',
        },
        {
          '-Tag': 'LOC_PROJECT_REPAIR_OUTER_DEFENSES_SHORT_NAME',
          Text: 'Repair Outer Defenses',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_EARTH_SATELLITE_NAME',
          Text: 'Launch Earth Satellite',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_EARTH_SATELLITE_SHORT_NAME',
          Text: 'Launch Earth Satellite',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MOON_LANDING_NAME',
          Text: 'Launch Moon Landing',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MOON_LANDING_SHORT_NAME',
          Text: 'Launch Moon Landing',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MARS_REACTOR_NAME',
          Text: 'Launch Mars Reactor',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MARS_REACTOR_SHORT_NAME',
          Text: 'Mars Colony Reactor',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MARS_HABITATION_NAME',
          Text: 'Launch Mars Habitation',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MARS_HABITATION_SHORT_NAME',
          Text: 'Mars Colony Habitation',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MARS_HYDROPONICS_NAME',
          Text: 'Launch Mars Hydroponics',
        },
        {
          '-Tag': 'LOC_PROJECT_LAUNCH_MARS_HYDROPONICS_SHORT_NAME',
          Text: 'Mars Colony Hydroponics',
        },
        {
          '-Tag': 'LOC_PROJECT_MANHATTAN_PROJECT_NAME',
          Text: 'Manhattan Project',
        },
        {
          '-Tag': 'LOC_PROJECT_MANHATTAN_PROJECT_SHORT_NAME',
          Text: 'Manhattan Project',
        },
        {
          '-Tag': 'LOC_PROJECT_OPERATION_IVY_NAME',
          Text: 'Operation Ivy',
        },
        {
          '-Tag': 'LOC_PROJECT_OPERATION_IVY_SHORT_NAME',
          Text: 'Operation Ivy',
        },
        {
          '-Tag': 'LOC_PROJECT_BUILD_NUCLEAR_DEVICE_NAME',
          Text: 'Build Nuclear Device',
        },
        {
          '-Tag': 'LOC_PROJECT_BUILD_NUCLEAR_DEVICE_SHORT_NAME',
          Text: 'Build Nuclear Device',
        },
        {
          '-Tag': 'LOC_PROJECT_BUILD_THERMONUCLEAR_DEVICE_NAME',
          Text: 'Build Thermonuclear Device',
        },
        {
          '-Tag': 'LOC_PROJECT_BUILD_THERMONUCLEAR_DEVICE_SHORT_NAME',
          Text: 'Build Thermonuclear Device',
        },
        {
          '-Tag': 'LOC_QUEST_CONVERT_CAPITAL_TO_RELIGION_NAME',
          Text: 'Religious Conversion',
        },
        {
          '-Tag': 'LOC_QUEST_SEND_TRADE_ROUTE_NAME',
          Text: 'Send Trade Route',
        },
        {
          '-Tag': 'LOC_QUEST_CLEAR_BARBARIAN_CAMP_NAME',
          Text: 'Destroy Barbarian Outpost within 5 tiles',
        },
        {
          '-Tag': 'LOC_QUEST_TRAIN_UNIT_TYPE_NAME',
          Text: 'Train Unit',
        },
        {
          '-Tag': 'LOC_QUEST_ZONE_DISTRICT_TYPE_NAME',
          Text: 'Construct District',
        },
        {
          '-Tag': 'LOC_QUEST_TRIGGER_TECH_BOOST_NAME',
          Text: 'Trigger Eureka',
        },
        {
          '-Tag': 'LOC_QUEST_TRIGGER_CIVIC_BOOST_NAME',
          Text: 'Trigger Inspiration',
        },
        {
          '-Tag': 'LOC_QUEST_RECRUIT_GREAT_PERSON_CLASS_NAME',
          Text: 'Recruit Great Person',
        },
        {
          '-Tag': 'LOC_RESOURCECLASS_BONUS_NAME',
          Text: 'Bonus',
        },
        {
          '-Tag': 'LOC_RESOURCECLASS_LUXURY_NAME',
          Text: 'Luxury',
        },
        {
          '-Tag': 'LOC_RESOURCECLASS_STRATEGIC_NAME',
          Text: 'Strategic',
        },
        {
          '-Tag': 'LOC_RESOURCECLASS_ARTIFACT_NAME',
          Text: 'Artifact',
        },
        {
          '-Tag': 'LOC_RESOURCE_BANANAS_NAME',
          Text: 'Bananas',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_CATTLE_NAME',
          Text: 'Cattle',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_COPPER_NAME',
          Text: 'Copper',
        },
        {
          '-Tag': 'LOC_RESOURCE_CRABS_NAME',
          Text: 'Crabs',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_DEER_NAME',
          Text: 'Deer',
        },
        {
          '-Tag': 'LOC_RESOURCE_FISH_NAME',
          Text: 'Fish',
        },
        {
          '-Tag': 'LOC_RESOURCE_RICE_NAME',
          Text: 'Rice',
        },
        {
          '-Tag': 'LOC_RESOURCE_SHEEP_NAME',
          Text: 'Sheep',
        },
        {
          '-Tag': 'LOC_RESOURCE_STONE_NAME',
          Text: 'Stone',
        },
        {
          '-Tag': 'LOC_RESOURCE_WHEAT_NAME',
          Text: 'Wheat',
        },
        {
          '-Tag': 'LOC_RESOURCE_CITRUS_NAME',
          Text: 'Citrus',
        },
        {
          '-Tag': 'LOC_RESOURCE_COCOA_NAME',
          Text: 'Cocoa',
        },
        {
          '-Tag': 'LOC_RESOURCE_COFFEE_NAME',
          Text: 'Coffee',
        },
        {
          '-Tag': 'LOC_RESOURCE_COTTON_NAME',
          Text: 'Cotton',
        },
        {
          '-Tag': 'LOC_RESOURCE_DIAMONDS_NAME',
          Text: 'Diamonds',
        },
        {
          '-Tag': 'LOC_RESOURCE_DYES_NAME',
          Text: 'Dyes',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_FURS_NAME',
          Text: 'Furs',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_GYPSUM_NAME',
          Text: 'Gypsum',
        },
        {
          '-Tag': 'LOC_RESOURCE_INCENSE_NAME',
          Text: 'Incense',
        },
        {
          '-Tag': 'LOC_RESOURCE_IVORY_NAME',
          Text: 'Ivory',
        },
        {
          '-Tag': 'LOC_RESOURCE_JADE_NAME',
          Text: 'Jade',
        },
        {
          '-Tag': 'LOC_RESOURCE_MARBLE_NAME',
          Text: 'Marble',
        },
        {
          '-Tag': 'LOC_RESOURCE_MERCURY_NAME',
          Text: 'Mercury',
        },
        {
          '-Tag': 'LOC_RESOURCE_PEARLS_NAME',
          Text: 'Pearls',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_SALT_NAME',
          Text: 'Salt',
        },
        {
          '-Tag': 'LOC_RESOURCE_SILK_NAME',
          Text: 'Silk',
        },
        {
          '-Tag': 'LOC_RESOURCE_SILVER_NAME',
          Text: 'Silver',
        },
        {
          '-Tag': 'LOC_RESOURCE_SPICES_NAME',
          Text: 'Spices',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_SUGAR_NAME',
          Text: 'Sugar',
        },
        {
          '-Tag': 'LOC_RESOURCE_TEA_NAME',
          Text: 'Tea',
        },
        {
          '-Tag': 'LOC_RESOURCE_TRUFFLES_NAME',
          Text: 'Truffles',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_TOBACCO_NAME',
          Text: 'Tobacco',
        },
        {
          '-Tag': 'LOC_RESOURCE_WHALES_NAME',
          Text: 'Whales',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_WINE_NAME',
          Text: 'Wine',
        },
        {
          '-Tag': 'LOC_RESOURCE_JEANS_NAME',
          Text: 'Jeans',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_PERFUME_NAME',
          Text: 'Perfume',
        },
        {
          '-Tag': 'LOC_RESOURCE_COSMETICS_NAME',
          Text: 'Cosmetics',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_TOYS_NAME',
          Text: 'Toys',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_CINNAMON_NAME',
          Text: 'Cinnamon',
        },
        {
          '-Tag': 'LOC_RESOURCE_CLOVES_NAME',
          Text: 'Cloves',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_ALUMINUM_NAME',
          Text: 'Aluminum',
        },
        {
          '-Tag': 'LOC_RESOURCE_COAL_NAME',
          Text: 'Coal',
        },
        {
          '-Tag': 'LOC_RESOURCE_HORSES_NAME',
          Text: 'Horses',
          Plurality: '2',
        },
        {
          '-Tag': 'LOC_RESOURCE_IRON_NAME',
          Text: 'Iron',
        },
        {
          '-Tag': 'LOC_RESOURCE_NITER_NAME',
          Text: 'Niter',
        },
        {
          '-Tag': 'LOC_RESOURCE_OIL_NAME',
          Text: 'Oil',
        },
        {
          '-Tag': 'LOC_RESOURCE_URANIUM_NAME',
          Text: 'Uranium',
        },
        {
          '-Tag': 'LOC_RESOURCE_ANTIQUITY_SITE_NAME',
          Text: 'Antiquity Site',
        },
        {
          '-Tag': 'LOC_RESOURCE_SHIPWRECK_NAME',
          Text: 'Shipwreck',
        },
        {
          '-Tag': 'LOC_ROUTE_ANCIENT_ROAD_NAME',
          Text: 'Ancient Road',
        },
        {
          '-Tag': 'LOC_ROUTE_MEDIEVAL_ROAD_NAME',
          Text: 'Classical Road',
        },
        {
          '-Tag': 'LOC_ROUTE_INDUSTRIAL_ROAD_NAME',
          Text: 'Industrial Road',
        },
        {
          '-Tag': 'LOC_ROUTE_MODERN_ROAD_NAME',
          Text: 'Modern Road',
        },
        {
          '-Tag': 'LOC_SLOT_ECONOMIC_NAME',
          Text: 'Economic',
        },
        {
          '-Tag': 'LOC_SLOT_MILITARY_NAME',
          Text: 'Military',
        },
        {
          '-Tag': 'LOC_SLOT_DIPLOMATIC_NAME',
          Text: 'Diplomatic',
        },
        {
          '-Tag': 'LOC_SLOT_GREAT_PERSON_NAME',
          Text: 'Great Person',
        },
        {
          '-Tag': 'LOC_SLOT_WILDCARD_NAME',
          Text: 'Wildcard',
        },
        {
          '-Tag': 'LOC_EMPTY_SLOT_ECONOMIC',
          Text: 'Empty Economic Policy Slot',
        },
        {
          '-Tag': 'LOC_EMPTY_SLOT_MILITARY',
          Text: 'Empty Military Policy Slot',
        },
        {
          '-Tag': 'LOC_EMPTY_SLOT_DIPLOMATIC',
          Text: 'Empty Diplomatic Policy Slot',
        },
        {
          '-Tag': 'LOC_EMPTY_SLOT_WILDCARD',
          Text: 'Empty Wildcard Slot',
        },
        {
          '-Tag': 'LOC_TECH_ADVANCED_BALLISTICS_NAME',
          Text: 'Advanced Ballistics',
        },
        {
          '-Tag': 'LOC_TECH_ADVANCED_FLIGHT_NAME',
          Text: 'Advanced Flight',
        },
        {
          '-Tag': 'LOC_TECH_ANIMAL_HUSBANDRY_NAME',
          Text: 'Animal Husbandry',
        },
        {
          '-Tag': 'LOC_TECH_ARCHERY_NAME',
          Text: 'Archery',
        },
        {
          '-Tag': 'LOC_TECH_ASTROLOGY_NAME',
          Text: 'Astrology',
        },
        {
          '-Tag': 'LOC_TECH_ASTRONOMY_NAME',
          Text: 'Astronomy',
        },
        {
          '-Tag': 'LOC_TECH_BALLISTICS_NAME',
          Text: 'Ballistics',
        },
        {
          '-Tag': 'LOC_TECH_BANKING_NAME',
          Text: 'Banking',
        },
        {
          '-Tag': 'LOC_TECH_BRONZE_WORKING_NAME',
          Text: 'Bronze Working',
        },
        {
          '-Tag': 'LOC_TECH_CARTOGRAPHY_NAME',
          Text: 'Cartography',
        },
        {
          '-Tag': 'LOC_TECH_CELESTIAL_NAVIGATION_NAME',
          Text: 'Celestial Navigation',
        },
        {
          '-Tag': 'LOC_TECH_CHEMISTRY_NAME',
          Text: 'Chemistry',
        },
        {
          '-Tag': 'LOC_TECH_COMBINED_ARMS_NAME',
          Text: 'Combined Arms',
        },
        {
          '-Tag': 'LOC_TECH_COMBUSTION_NAME',
          Text: 'Combustion',
        },
        {
          '-Tag': 'LOC_TECH_COMPOSITES_NAME',
          Text: 'Composites',
        },
        {
          '-Tag': 'LOC_TECH_COMPUTERS_NAME',
          Text: 'Computers',
        },
        {
          '-Tag': 'LOC_TECH_CONSTRUCTION_NAME',
          Text: 'Construction',
        },
        {
          '-Tag': 'LOC_TECH_CURRENCY_NAME',
          Text: 'Currency',
        },
        {
          '-Tag': 'LOC_TECH_ECONOMICS_NAME',
          Text: 'Economics',
        },
        {
          '-Tag': 'LOC_TECH_EDUCATION_NAME',
          Text: 'Education',
        },
        {
          '-Tag': 'LOC_TECH_ELECTRICITY_NAME',
          Text: 'Electricity',
        },
        {
          '-Tag': 'LOC_TECH_ENGINEERING_NAME',
          Text: 'Engineering',
        },
        {
          '-Tag': 'LOC_TECH_FLIGHT_NAME',
          Text: 'Flight',
        },
        {
          '-Tag': 'LOC_TECH_CASTLES_NAME',
          Text: 'Castles',
        },
        {
          '-Tag': 'LOC_TECH_FUTURE_TECH_NAME',
          Text: 'Future Tech',
        },
        {
          '-Tag': 'LOC_TECH_GUIDANCE_SYSTEMS_NAME',
          Text: 'Guidance Systems',
        },
        {
          '-Tag': 'LOC_TECH_GUNPOWDER_NAME',
          Text: 'Gunpowder',
        },
        {
          '-Tag': 'LOC_TECH_HORSEBACK_RIDING_NAME',
          Text: 'Horseback Riding',
        },
        {
          '-Tag': 'LOC_TECH_INDUSTRIALIZATION_NAME',
          Text: 'Industrialization',
        },
        {
          '-Tag': 'LOC_TECH_IRON_WORKING_NAME',
          Text: 'Iron Working',
        },
        {
          '-Tag': 'LOC_TECH_IRRIGATION_NAME',
          Text: 'Irrigation',
        },
        {
          '-Tag': 'LOC_TECH_LASERS_NAME',
          Text: 'Lasers',
        },
        {
          '-Tag': 'LOC_TECH_MACHINERY_NAME',
          Text: 'Machinery',
        },
        {
          '-Tag': 'LOC_TECH_MASONRY_NAME',
          Text: 'Masonry',
        },
        {
          '-Tag': 'LOC_TECH_MASS_PRODUCTION_NAME',
          Text: 'Mass Production',
        },
        {
          '-Tag': 'LOC_TECH_MATHEMATICS_NAME',
          Text: 'Mathematics',
        },
        {
          '-Tag': 'LOC_TECH_APPRENTICESHIP_NAME',
          Text: 'Apprenticeship',
        },
        {
          '-Tag': 'LOC_TECH_METAL_CASTING_NAME',
          Text: 'Metal Casting',
        },
        {
          '-Tag': 'LOC_TECH_MILITARY_ENGINEERING_NAME',
          Text: 'Military Engineering',
        },
        {
          '-Tag': 'LOC_TECH_STIRRUPS_NAME',
          Text: 'Stirrups',
        },
        {
          '-Tag': 'LOC_TECH_MILITARY_SCIENCE_NAME',
          Text: 'Military Science',
        },
        {
          '-Tag': 'LOC_TECH_MILITARY_TACTICS_NAME',
          Text: 'Military Tactics',
        },
        {
          '-Tag': 'LOC_TECH_MINING_NAME',
          Text: 'Mining',
        },
        {
          '-Tag': 'LOC_TECH_NANOTECHNOLOGY_NAME',
          Text: 'Nanotechnology',
        },
        {
          '-Tag': 'LOC_TECH_NUCLEAR_FISSION_NAME',
          Text: 'Nuclear Fission',
        },
        {
          '-Tag': 'LOC_TECH_NUCLEAR_FUSION_NAME',
          Text: 'Nuclear Fusion',
        },
        {
          '-Tag': 'LOC_TECH_POTTERY_NAME',
          Text: 'Pottery',
        },
        {
          '-Tag': 'LOC_TECH_PLASTICS_NAME',
          Text: 'Plastics',
        },
        {
          '-Tag': 'LOC_TECH_PRINTING_NAME',
          Text: 'Printing',
        },
        {
          '-Tag': 'LOC_TECH_RADIO_NAME',
          Text: 'Radio',
        },
        {
          '-Tag': 'LOC_TECH_REPLACEABLE_PARTS_NAME',
          Text: 'Replaceable Parts',
        },
        {
          '-Tag': 'LOC_TECH_RIFLING_NAME',
          Text: 'Rifling',
        },
        {
          '-Tag': 'LOC_TECH_ROBOTICS_NAME',
          Text: 'Robotics',
        },
        {
          '-Tag': 'LOC_TECH_ROCKETRY_NAME',
          Text: 'Rocketry',
        },
        {
          '-Tag': 'LOC_TECH_SAILING_NAME',
          Text: 'Sailing',
        },
        {
          '-Tag': 'LOC_TECH_SANITATION_NAME',
          Text: 'Sanitation',
        },
        {
          '-Tag': 'LOC_TECH_SATELLITES_NAME',
          Text: 'Satellites',
        },
        {
          '-Tag': 'LOC_TECH_SCIENTIFIC_THEORY_NAME',
          Text: 'Scientific Theory',
        },
        {
          '-Tag': 'LOC_TECH_SHIPBUILDING_NAME',
          Text: 'Shipbuilding',
        },
        {
          '-Tag': 'LOC_TECH_SIEGE_TACTICS_NAME',
          Text: 'Siege Tactics',
        },
        {
          '-Tag': 'LOC_TECH_SQUARE_RIGGING_NAME',
          Text: 'Square Rigging',
        },
        {
          '-Tag': 'LOC_TECH_STEALTH_TECHNOLOGY_NAME',
          Text: 'Stealth Technology',
        },
        {
          '-Tag': 'LOC_TECH_STEAM_POWER_NAME',
          Text: 'Steam Power',
        },
        {
          '-Tag': 'LOC_TECH_STEEL_NAME',
          Text: 'Steel',
        },
        {
          '-Tag': 'LOC_TECH_SYNTHETIC_MATERIALS_NAME',
          Text: 'Synthetic Materials',
        },
        {
          '-Tag': 'LOC_TECH_TELECOMMUNICATIONS_NAME',
          Text: 'Telecommunications',
        },
        {
          '-Tag': 'LOC_TECH_THE_WHEEL_NAME',
          Text: 'Wheel',
        },
        {
          '-Tag': 'LOC_TECH_WRITING_NAME',
          Text: 'Writing',
        },
        {
          '-Tag': 'LOC_TERRAIN_COAST_NAME',
          Text: 'Coast and Lake',
        },
        {
          '-Tag': 'LOC_TERRAIN_DESERT_NAME',
          Text: 'Desert',
        },
        {
          '-Tag': 'LOC_TERRAIN_DESERT_HILLS_NAME',
          Text: 'Desert (Hills)',
        },
        {
          '-Tag': 'LOC_TERRAIN_DESERT_MOUNTAIN_NAME',
          Text: 'Desert (Mountain)',
        },
        {
          '-Tag': 'LOC_TERRAIN_GRASS_NAME',
          Text: 'Grassland',
        },
        {
          '-Tag': 'LOC_TERRAIN_GRASS_HILLS_NAME',
          Text: 'Grassland (Hills)',
        },
        {
          '-Tag': 'LOC_TERRAIN_GRASS_MOUNTAIN_NAME',
          Text: 'Grassland (Mountain)',
        },
        {
          '-Tag': 'LOC_TERRAIN_OCEAN_NAME',
          Text: 'Ocean',
        },
        {
          '-Tag': 'LOC_TERRAIN_PLAINS_NAME',
          Text: 'Plains',
        },
        {
          '-Tag': 'LOC_TERRAIN_PLAINS_HILLS_NAME',
          Text: 'Plains (Hills)',
        },
        {
          '-Tag': 'LOC_TERRAIN_PLAINS_MOUNTAIN_NAME',
          Text: 'Plains (Mountain)',
        },
        {
          '-Tag': 'LOC_TERRAIN_SNOW_NAME',
          Text: 'Snow',
        },
        {
          '-Tag': 'LOC_TERRAIN_SNOW_HILLS_NAME',
          Text: 'Snow (Hills)',
        },
        {
          '-Tag': 'LOC_TERRAIN_SNOW_MOUNTAIN_NAME',
          Text: 'Snow (Mountain)',
        },
        {
          '-Tag': 'LOC_TERRAIN_TUNDRA_NAME',
          Text: 'Tundra',
        },
        {
          '-Tag': 'LOC_TERRAIN_TUNDRA_HILLS_NAME',
          Text: 'Tundra (Hills)',
        },
        {
          '-Tag': 'LOC_TERRAIN_TUNDRA_MOUNTAIN_NAME',
          Text: 'Tundra (Mountain)',
        },
        {
          '-Tag': 'LOC_TERRAIN_OUTOFPLAY_NAME',
          Text: 'Void Hex',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_GRASS_NAME',
          Text: 'Grass Class',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_MOUNTAIN_NAME',
          Text: 'Mountain Class',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_PLAINS_NAME',
          Text: 'Plains Class',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_DESERT_NAME',
          Text: 'Desert Class',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_TUNDRA_NAME',
          Text: 'Tundra Class',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_SNOW_NAME',
          Text: 'Snow Class',
        },
        {
          '-Tag': 'LOC_TERRAIN_CLASS_WATER_NAME',
          Text: 'Water Class',
        },
        {
          '-Tag': 'LOC_UNIT_AIRCRAFT_CARRIER_NAME',
          Text: 'Aircraft Carrier',
          Gender: 'masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_ANTIAIR_GUN_NAME',
          Text: 'Anti-Air Gun',
          Gender: 'masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_APOSTLE_NAME',
          Text: 'Apostle',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_ARCHAEOLOGIST_NAME',
          Text: 'Archaeologist',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_ARCHER_NAME',
          Text: 'Archer',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_ARTILLERY_NAME',
          Text: 'Artillery',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_AT_CREW_NAME',
          Text: 'AT Crew',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_BARBARIAN_HORSEMAN_NAME',
          Text: 'Barbarian Horseman',
        },
        {
          '-Tag': 'LOC_UNIT_BARBARIAN_HORSE_ARCHER_NAME',
          Text: 'Barbarian Horse Archer',
        },
        {
          '-Tag': 'LOC_UNIT_BARBARIAN_GALLEY_NAME',
          Text: 'Barbarian Galley',
        },
        {
          '-Tag': 'LOC_UNIT_BATTERING_RAM_NAME',
          Text: 'Battering Ram',
        },
        {
          '-Tag': 'LOC_UNIT_BATTLESHIP_NAME',
          Text: 'Battleship',
        },
        {
          '-Tag': 'LOC_UNIT_BUILDER_NAME',
          Text: 'Builder',
        },
        {
          '-Tag': 'LOC_UNIT_BIPLANE_NAME',
          Text: 'Biplane',
        },
        {
          '-Tag': 'LOC_UNIT_BOMBER_NAME',
          Text: 'Bomber',
        },
        {
          '-Tag': 'LOC_UNIT_BOMBARD_NAME',
          Text: 'Bombard',
        },
        {
          '-Tag': 'LOC_UNIT_CARAVEL_NAME',
          Text: 'Caravel',
        },
        {
          '-Tag': 'LOC_UNIT_CATAPULT_NAME',
          Text: 'Catapult',
        },
        {
          '-Tag': 'LOC_UNIT_CAVALRY_NAME',
          Text: 'Cavalry',
        },
        {
          '-Tag': 'LOC_UNIT_DESTROYER_NAME',
          Text: 'Destroyer',
        },
        {
          '-Tag': 'LOC_UNIT_CROSSBOWMAN_NAME',
          Text: 'Crossbowman',
        },
        {
          '-Tag': 'LOC_UNIT_FIELD_CANNON_NAME',
          Text: 'Field Cannon',
        },
        {
          '-Tag': 'LOC_UNIT_FIGHTER_NAME',
          Text: 'Fighter',
        },
        {
          '-Tag': 'LOC_UNIT_FRIGATE_NAME',
          Text: 'Frigate',
        },
        {
          '-Tag': 'LOC_UNIT_GALLEY_NAME',
          Text: 'Galley',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_GENERAL_NAME',
          Text: 'Great General',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_GENERAL_NAME',
          Text: 'Great General',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_ADMIRAL_NAME',
          Text: 'Great Admiral',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_ADMIRAL_NAME',
          Text: 'Great Admiral',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_ENGINEER_NAME',
          Text: 'Great Engineer',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_ENGINEER_NAME',
          Text: 'Great Engineer',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_MERCHANT_NAME',
          Text: 'Great Merchant',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_MERCHANT_NAME',
          Text: 'Great Merchant',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_PROPHET_NAME',
          Text: 'Great Prophet',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_PROPHET_NAME',
          Text: 'Great Prophet',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_SCIENTIST_NAME',
          Text: 'Great Scientist',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_SCIENTIST_NAME',
          Text: 'Great Scientist',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_WRITER_NAME',
          Text: 'Great Writer',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_WRITER_NAME',
          Text: 'Great Writer',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_ARTIST_NAME',
          Text: 'Great Artist',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_ARTIST_NAME',
          Text: 'Great Artist',
        },
        {
          '-Tag': 'LOC_GREAT_PERSON_CLASS_MUSICIAN_NAME',
          Text: 'Great Musician',
        },
        {
          '-Tag': 'LOC_UNIT_GREAT_MUSICIAN_NAME',
          Text: 'Great Musician',
        },
        {
          '-Tag': 'LOC_UNIT_GURU_NAME',
          Text: 'Guru',
        },
        {
          '-Tag': 'LOC_UNIT_HEAVY_CHARIOT_NAME',
          Text: 'Heavy Chariot',
        },
        {
          '-Tag': 'LOC_UNIT_HELICOPTER_NAME',
          Text: 'Helicopter',
        },
        {
          '-Tag': 'LOC_UNIT_HORSEMAN_NAME',
          Text: 'Horseman',
        },
        {
          '-Tag': 'LOC_UNIT_INFANTRY_NAME',
          Text: 'Infantry',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_INQUISITOR_NAME',
          Text: 'Inquisitor',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_IRONCLAD_NAME',
          Text: 'Ironclad',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_JET_BOMBER_NAME',
          Text: 'Jet Bomber',
        },
        {
          '-Tag': 'LOC_UNIT_JET_FIGHTER_NAME',
          Text: 'Jet Fighter',
        },
        {
          '-Tag': 'LOC_UNIT_KNIGHT_NAME',
          Text: 'Knight',
        },
        {
          '-Tag': 'LOC_UNIT_MACHINE_GUN_NAME',
          Text: 'Machine Gun',
        },
        {
          '-Tag': 'LOC_UNIT_MECHANIZED_INFANTRY_NAME',
          Text: 'Mechanized Infantry',
        },
        {
          '-Tag': 'LOC_UNIT_MEDIC_NAME',
          Text: 'Medic',
        },
        {
          '-Tag': 'LOC_UNIT_MILITARY_ENGINEER_NAME',
          Text: 'Military Engineer',
        },
        {
          '-Tag': 'LOC_UNIT_MISSILE_CRUISER_NAME',
          Text: 'Missile Cruiser',
        },
        {
          '-Tag': 'LOC_UNIT_MISSIONARY_NAME',
          Text: 'Missionary',
        },
        {
          '-Tag': 'LOC_UNIT_MODERN_AT_NAME',
          Text: 'Modern AT',
        },
        {
          '-Tag': 'LOC_UNIT_MODERN_ARMOR_NAME',
          Text: 'Modern Armor',
        },
        {
          '-Tag': 'LOC_UNIT_MUSKETMAN_NAME',
          Text: 'Musketman',
        },
        {
          '-Tag': 'LOC_UNIT_NATURALIST_NAME',
          Text: 'Naturalist',
        },
        {
          '-Tag': 'LOC_UNIT_NUCLEAR_SUBMARINE_NAME',
          Text: 'Nuclear Submarine',
        },
        {
          '-Tag': 'LOC_UNIT_OBSERVATION_BALLOON_NAME',
          Text: 'Observation Balloon',
          Gender: 'Masculine:an',
        },
        {
          '-Tag': 'LOC_UNIT_PIKEMAN_NAME',
          Text: 'Pikeman',
        },
        {
          '-Tag': 'LOC_UNIT_PRIVATEER_NAME',
          Text: 'Privateer',
        },
        {
          '-Tag': 'LOC_UNIT_QUADRIREME_NAME',
          Text: 'Quadrireme',
        },
        {
          '-Tag': 'LOC_UNIT_RANGER_NAME',
          Text: 'Ranger',
        },
        {
          '-Tag': 'LOC_UNIT_ROCKET_ARTILLERY_NAME',
          Text: 'Rocket Artillery',
        },
        {
          '-Tag': 'LOC_UNIT_MOBILE_SAM_NAME',
          Text: 'Mobile SAM',
        },
        {
          '-Tag': 'LOC_UNIT_SCOUT_NAME',
          Text: 'Scout',
        },
        {
          '-Tag': 'LOC_UNIT_SETTLER_NAME',
          Text: 'Settler',
        },
        {
          '-Tag': 'LOC_UNIT_SIEGE_TOWER_NAME',
          Text: 'Siege Tower',
        },
        {
          '-Tag': 'LOC_UNIT_SLINGER_NAME',
          Text: 'Slinger',
        },
        {
          '-Tag': 'LOC_UNIT_SPEARMAN_NAME',
          Text: 'Spearman',
        },
        {
          '-Tag': 'LOC_UNIT_SPY_NAME',
          Text: 'Spy',
        },
        {
          '-Tag': 'LOC_UNIT_SUBMARINE_NAME',
          Text: 'Submarine',
        },
        {
          '-Tag': 'LOC_UNIT_SWORDSMAN_NAME',
          Text: 'Swordsman',
        },
        {
          '-Tag': 'LOC_UNIT_TANK_NAME',
          Text: 'Tank',
        },
        {
          '-Tag': 'LOC_UNIT_TRADER_NAME',
          Text: 'Trader',
        },
        {
          '-Tag': 'LOC_UNIT_WARRIOR_NAME',
          Text: 'Warrior',
        },
        {
          '-Tag': 'LOC_UNIT_WARRIOR_MONK_NAME',
          Text: 'Warrior Monk',
        },
        {
          '-Tag': 'LOC_UNIT_EGYPTIAN_CHARIOT_ARCHER_NAME',
          Text: 'Maryannu Chariot Archer',
        },
        {
          '-Tag': 'LOC_UNIT_GREEK_HOPLITE_NAME',
          Text: 'Hoplite',
        },
        {
          '-Tag': 'LOC_UNIT_SCYTHIAN_HORSE_ARCHER_NAME',
          Text: 'Saka Horse Archer',
        },
        {
          '-Tag': 'LOC_UNIT_SUMERIAN_WAR_CART_NAME',
          Text: 'War-Cart',
        },
        {
          '-Tag': 'LOC_UNIT_AMERICAN_ROUGH_RIDER_NAME',
          Text: 'Rough Rider',
        },
        {
          '-Tag': 'LOC_UNIT_ENGLISH_REDCOAT_NAME',
          Text: 'Redcoat',
        },
        {
          '-Tag': 'LOC_UNIT_SPANISH_CONQUISTADOR_NAME',
          Text: 'Conquistador',
        },
        {
          '-Tag': 'LOC_UNIT_ROMAN_LEGION_NAME',
          Text: 'Legion',
        },
        {
          '-Tag': 'LOC_UNIT_JAPANESE_SAMURAI_NAME',
          Text: 'Samurai',
        },
        {
          '-Tag': 'LOC_UNIT_NORWEGIAN_BERSERKER_NAME',
          Text: 'Berserker',
        },
        {
          '-Tag': 'LOC_UNIT_NORWEGIAN_LONGSHIP_NAME',
          Text: 'Viking Longship',
        },
        {
          '-Tag': 'LOC_UNIT_CHINESE_CROUCHING_TIGER_NAME',
          Text: 'Crouching Tiger',
        },
        {
          '-Tag': 'LOC_UNIT_RUSSIAN_COSSACK_NAME',
          Text: 'Cossack',
        },
        {
          '-Tag': 'LOC_UNIT_INDIAN_VARU_NAME',
          Text: 'Varu',
        },
        {
          '-Tag': 'LOC_UNIT_ARABIAN_MAMLUK_NAME',
          Text: 'Mamluk',
        },
        {
          '-Tag': 'LOC_UNIT_ENGLISH_SEADOG_NAME',
          Text: 'Sea Dog',
        },
        {
          '-Tag': 'LOC_UNIT_KONGO_SHIELD_BEARER_NAME',
          Text: 'Ngao Mbeba',
        },
        {
          '-Tag': 'LOC_UNIT_FRENCH_GARDE_IMPERIALE_NAME',
          Text: 'Garde Impériale',
        },
        {
          '-Tag': 'LOC_UNIT_GERMAN_UBOAT_NAME',
          Text: 'U-Boat',
        },
        {
          '-Tag': 'LOC_UNIT_AMERICAN_P51_NAME',
          Text: 'P-51 Mustang',
        },
        {
          '-Tag': 'LOC_UNIT_BRAZILIAN_MINAS_GERAES_NAME',
          Text: 'Minas Geraes',
        },
        {
          '-Tag': 'LOC_WMD_NUCLEAR_DEVICE_NAME',
          Text: 'Nuclear Device',
        },
        {
          '-Tag': 'LOC_WMD_THERMONUCLEAR_DEVICE_NAME',
          Text: 'Thermonuclear Device',
        },
        {
          '-Tag': 'LOC_YIELD_CULTURE_NAME',
          Text: 'Culture',
        },
        {
          '-Tag': 'LOC_YIELD_FAITH_NAME',
          Text: 'Faith',
        },
        {
          '-Tag': 'LOC_YIELD_FOOD_NAME',
          Text: 'Food',
        },
        {
          '-Tag': 'LOC_YIELD_GOLD_NAME',
          Text: 'Gold',
        },
        {
          '-Tag': 'LOC_YIELD_PRODUCTION_NAME',
          Text: 'Production',
        },
        {
          '-Tag': 'LOC_YIELD_SCIENCE_NAME',
          Text: 'Science',
        },
        {
          '-Tag': 'LOC_CIVILIZATION_AZTEC_NAME',
          Text: 'Aztec',
        },
        {
          '-Tag': 'LOC_LEADER_MONTEZUMA_NAME',
          Text: 'Montezuma',
        },
      ],
    },
  },
};
