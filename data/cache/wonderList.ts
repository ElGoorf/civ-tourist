export default [{
  name: 'Alhambra',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_alhambra',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_GENERAL: 2 },
  greatWorksSlots: {},
  modifiers: {
    ALHAMBRA_MILITARY_GOVERNMENT_SLOT: [{
      ModifierId: 'ALHAMBRA_MILITARY_GOVERNMENT_SLOT',
      Name: 'GovernmentSlotType',
      Value: 'SLOT_MILITARY',
    }],
  },
}, {
  name: 'Big Ben',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_big_ben',
  yields: { YIELD_GOLD: 6 },
  greatPersonPoints: { GREAT_PERSON_CLASS_MERCHANT: 3 },
  greatWorksSlots: {},
  modifiers: {
    BIG_BEN_ECONOMIC_GOVERNMENT_SLOT: [{
      ModifierId: 'BIG_BEN_ECONOMIC_GOVERNMENT_SLOT',
      Name: 'GovernmentSlotType',
      Value: 'SLOT_ECONOMIC',
    }],
    BIG_BEN_DOUBLE_GOLD: [{ ModifierId: 'BIG_BEN_DOUBLE_GOLD', Name: 'Amount', Value: '100' }],
  },
}, {
  name: 'Bolshoi Theatre',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_bolshoi_theatre',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_WRITER: 2, GREAT_PERSON_CLASS_MUSICIAN: 2 },
  greatWorksSlots: { GREATWORKSLOT_WRITING: 1, GREATWORKSLOT_MUSIC: 1 },
  modifiers: {
    BOLSHOI_THEATRE_FREE_CIVICS: [{
      ModifierId: 'BOLSHOI_THEATRE_FREE_CIVICS',
      Name: 'Amount',
      Value: '2',
    }],
  },
}, {
  name: 'Broadway',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_broadway',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_WRITER: 3, GREAT_PERSON_CLASS_MUSICIAN: 3 },
  greatWorksSlots: { GREATWORKSLOT_WRITING: 1, GREATWORKSLOT_MUSIC: 2 },
  modifiers: {
    BROADWAY_ADDCULTUREYIELD: [{
      ModifierId: 'BROADWAY_ADDCULTUREYIELD',
      Name: 'Amount',
      Value: '20',
    }, { ModifierId: 'BROADWAY_ADDCULTUREYIELD', Name: 'YieldType', Value: 'YIELD_CULTURE' }],
    BUILDING_BROADWAY_RANDOMCIVICBOOST: [{
      ModifierId: 'BUILDING_BROADWAY_RANDOMCIVICBOOST',
      Name: 'StartEraType',
      Value: 'ERA_ATOMIC',
    }, {
      ModifierId: 'BUILDING_BROADWAY_RANDOMCIVICBOOST',
      Name: 'EndEraType',
      Value: 'ERA_ATOMIC',
    }, { ModifierId: 'BUILDING_BROADWAY_RANDOMCIVICBOOST', Name: 'Amount', Value: '1' }],
  },
}, {
  name: 'Chichen Itza',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_chichen_itza',
  yields: {},
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    CHICHEN_ITZA_JUNGLE_CULTURE: [{
      ModifierId: 'CHICHEN_ITZA_JUNGLE_CULTURE',
      Name: 'ModifierId',
      Value: 'CHICHEN_ITZA_JUNGLE_CULTURE_MODIFIER',
    }],
    CHICHEN_ITZA_JUNGLE_PRODUCTION: [{
      ModifierId: 'CHICHEN_ITZA_JUNGLE_PRODUCTION',
      Name: 'ModifierId',
      Value: 'CHICHEN_ITZA_JUNGLE_PRODUCTION_MODIFIER',
    }],
  },
}, {
  name: 'Colosseum',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_colosseum',
  yields: { YIELD_CULTURE: 2 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {},
}, {
  name: 'Colossus',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_colossus',
  yields: { YIELD_GOLD: 3 },
  greatPersonPoints: { GREAT_PERSON_CLASS_ADMIRAL: 1 },
  greatWorksSlots: {},
  modifiers: {
    COLOSSUS_ADDTRADEROUTE: [{
      ModifierId: 'COLOSSUS_ADDTRADEROUTE',
      Name: 'Amount',
      Value: '1',
    }],
    COLOSSUS_GRANT_TRADER: [{
      ModifierId: 'COLOSSUS_GRANT_TRADER',
      Name: 'UnitType',
      Value: 'UNIT_TRADER',
    }, { ModifierId: 'COLOSSUS_GRANT_TRADER', Name: 'Amount', Value: '1' }],
  },
}, {
  name: 'Cristo Redentor',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_cristo_redentor',
  yields: { YIELD_CULTURE: 4 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    CRISTOREDENTOR_BEACHTOURISM: [{
      ModifierId: 'CRISTOREDENTOR_BEACHTOURISM',
      Name: 'ImprovementType',
      Value: 'IMPROVEMENT_BEACH_RESORT',
    }, { ModifierId: 'CRISTOREDENTOR_BEACHTOURISM', Name: 'ScalingFactor', Value: '200' }],
    CRISTOREDENTOR_FULLRELIGIOUSTOURISM: [{
      ModifierId: 'CRISTOREDENTOR_FULLRELIGIOUSTOURISM',
      Name: 'Enable',
      Value: 'true',
    }],
  },
}, {
  name: 'Eiffel Tower',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_eiffel_tower',
  yields: {},
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    EIFFEL_TOWER_ADDAPPEAL: [{
      ModifierId: 'EIFFEL_TOWER_ADDAPPEAL',
      Name: 'Amount',
      Value: '2',
    }],
  },
}, {
  name: 'Estádio do Maracanã',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_estadio_do_maracana',
  yields: { YIELD_CULTURE: 6 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {},
}, {
  name: 'Forbidden City',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_forbidden_city',
  yields: { YIELD_CULTURE: 5 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    FORBIDDEN_CITY_WILDCARD_GOVERNMENT_SLOT: [{
      ModifierId: 'FORBIDDEN_CITY_WILDCARD_GOVERNMENT_SLOT',
      Name: 'GovernmentSlotType',
      Value: 'SLOT_WILDCARD',
    }],
  },
}, {
  name: 'Great Library',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_great_library',
  yields: { YIELD_SCIENCE: 2 },
  greatPersonPoints: { GREAT_PERSON_CLASS_SCIENTIST: 1 },
  greatWorksSlots: { GREATWORKSLOT_WRITING: 2 },
  modifiers: {
    GREAT_LIBRARY_ANCIENT_CLASSICAL_TECH_BOOSTS: [{
      ModifierId: 'GREAT_LIBRARY_ANCIENT_CLASSICAL_TECH_BOOSTS',
      Name: 'StartEraType',
      Value: 'ERA_ANCIENT',
    }, {
      ModifierId: 'GREAT_LIBRARY_ANCIENT_CLASSICAL_TECH_BOOSTS',
      Name: 'EndEraType',
      Value: 'ERA_CLASSICAL',
    }],
  },
}, {
  name: 'Great Lighthouse',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_great_lighthouse',
  yields: { YIELD_GOLD: 3 },
  greatPersonPoints: { GREAT_PERSON_CLASS_ADMIRAL: 1 },
  greatWorksSlots: {},
  modifiers: {
    GREATLIGHTHOUSE_ADJUST_SEA_MOVEMENT: [{
      ModifierId: 'GREATLIGHTHOUSE_ADJUST_SEA_MOVEMENT',
      Name: 'AbilityType',
      Value: 'ABILITY_GREAT_LIGHTHOUSE_MOVEMENT',
    }],
    GREATLIGHTHOUSE_ADJUST_EMBARKED_MOVEMENT: [{
      ModifierId: 'GREATLIGHTHOUSE_ADJUST_EMBARKED_MOVEMENT',
      Name: 'Amount',
      Value: '1',
    }],
    GREATLIGHTHOUSE_ADD_EMBARKED_ABILITY_TEXT_ONLY: [{
      ModifierId: 'GREATLIGHTHOUSE_ADD_EMBARKED_ABILITY_TEXT_ONLY',
      Name: 'AbilityType',
      Value: 'ABILITY_GREAT_LIGHTHOUSE_EMBARKED_MOVEMENT',
    }],
  },
}, {
  name: 'Great Zimbabwe',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_great_zimbabwe',
  yields: { YIELD_GOLD: 5 },
  greatPersonPoints: { GREAT_PERSON_CLASS_MERCHANT: 2 },
  greatWorksSlots: {},
  modifiers: {
    GREAT_ZIMBABWE_ADDTRADEROUTE: [{
      ModifierId: 'GREAT_ZIMBABWE_ADDTRADEROUTE',
      Name: 'Amount',
      Value: '1',
    }],
    GREAT_ZIMBABWE_DOMESTICBONUSRESOURCEGOLD: [{
      ModifierId: 'GREAT_ZIMBABWE_DOMESTICBONUSRESOURCEGOLD',
      Name: 'YieldType',
      Value: 'YIELD_GOLD',
    }, { ModifierId: 'GREAT_ZIMBABWE_DOMESTICBONUSRESOURCEGOLD', Name: 'Amount', Value: '2' }],
    GREAT_ZIMBABWE_INTERNATIONALBONUSRESOURCEGOLD: [{
      ModifierId: 'GREAT_ZIMBABWE_INTERNATIONALBONUSRESOURCEGOLD',
      Name: 'YieldType',
      Value: 'YIELD_GOLD',
    }, {
      ModifierId: 'GREAT_ZIMBABWE_INTERNATIONALBONUSRESOURCEGOLD',
      Name: 'Amount',
      Value: '2',
    }],
  },
}, {
  name: 'Hagia Sophia',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_hagia_sophia',
  yields: { YIELD_FAITH: 4 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    HAGIA_SOPHIA_ADJUST_RELIGIOUS_CHARGES: [{
      ModifierId: 'HAGIA_SOPHIA_ADJUST_RELIGIOUS_CHARGES',
      Name: 'Amount',
      Value: '1',
    }],
  },
}, {
  name: 'Hanging Gardens',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_hanging_gardens',
  yields: {},
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    HANGING_GARDEN_ADDGROWTH: [{
      ModifierId: 'HANGING_GARDEN_ADDGROWTH',
      Name: 'Amount',
      Value: '15',
    }],
  },
}, {
  name: 'Hermitage',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_hermitage',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_ARTIST: 3 },
  greatWorksSlots: { GREATWORKSLOT_ART: 4 },
  modifiers: {},
}, {
  name: 'Mahabodhi Temple',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_mahabodhi_temple',
  yields: { YIELD_FAITH: 4 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    MAHABODHITEMPLE_APOSTLE: [{
      ModifierId: 'MAHABODHITEMPLE_APOSTLE',
      Name: 'UnitType',
      Value: 'UNIT_APOSTLE',
    }, { ModifierId: 'MAHABODHITEMPLE_APOSTLE', Name: 'Amount', Value: '2' }],
  },
}, {
  name: 'Mont St. Michel',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_mont_st_michel',
  yields: { YIELD_FAITH: 2 },
  greatPersonPoints: {},
  greatWorksSlots: { GREATWORKSLOT_RELIC: 2 },
  modifiers: {
    MONT_ST_MICHEL_GRANT_MARTYR: [{
      ModifierId: 'MONT_ST_MICHEL_GRANT_MARTYR',
      Name: 'PromotionType',
      Value: 'PROMOTION_MARTYR',
    }],
  },
}, {
  name: 'Oracle',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_oracle',
  yields: { YIELD_CULTURE: 1, YIELD_FAITH: 1 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    ORACLE_GREATGENERALPOINTS: [{
      ModifierId: 'ORACLE_GREATGENERALPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_GENERAL',
    }, { ModifierId: 'ORACLE_GREATGENERALPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_GREATADMIRALPOINTS: [{
      ModifierId: 'ORACLE_GREATADMIRALPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_ADMIRAL',
    }, { ModifierId: 'ORACLE_GREATADMIRALPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_GREATENGINEERPOINTS: [{
      ModifierId: 'ORACLE_GREATENGINEERPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_ENGINEER',
    }, { ModifierId: 'ORACLE_GREATENGINEERPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_GREATMERCHANTPOINTS: [{
      ModifierId: 'ORACLE_GREATMERCHANTPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_MERCHANT',
    }, { ModifierId: 'ORACLE_GREATMERCHANTPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_GREATPROPHETPOINTS: [{
      ModifierId: 'ORACLE_GREATPROPHETPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_PROPHET',
    }, { ModifierId: 'ORACLE_GREATPROPHETPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_GREATSCIENTISTPOINTS: [{
      ModifierId: 'ORACLE_GREATSCIENTISTPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_SCIENTIST',
    }, { ModifierId: 'ORACLE_GREATSCIENTISTPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_GREATWRITERPOINTS: [{
      ModifierId: 'ORACLE_GREATWRITERPOINTS',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_WRITER',
    }, { ModifierId: 'ORACLE_GREATWRITERPOINTS', Name: 'Amount', Value: '2' }],
    ORACLE_PATRONAGE_FAITH_DISCOUNT: [{
      ModifierId: 'ORACLE_PATRONAGE_FAITH_DISCOUNT',
      Name: 'YieldType',
      Value: 'YIELD_FAITH',
    }, { ModifierId: 'ORACLE_PATRONAGE_FAITH_DISCOUNT', Name: 'Amount', Value: '25' }],
  },
}, {
  name: 'Oxford University',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_oxford_university',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_SCIENTIST: 3 },
  greatWorksSlots: { GREATWORKSLOT_WRITING: 2 },
  modifiers: {
    OXFORD_ADDSCIENCEYIELD: [{
      ModifierId: 'OXFORD_ADDSCIENCEYIELD',
      Name: 'Amount',
      Value: '20',
    }, { ModifierId: 'OXFORD_ADDSCIENCEYIELD', Name: 'YieldType', Value: 'YIELD_SCIENCE' }],
    OXFORD_UNIVERSITY_FREE_TECHS: [{
      ModifierId: 'OXFORD_UNIVERSITY_FREE_TECHS',
      Name: 'Amount',
      Value: '2',
    }],
  },
}, {
  name: 'Petra',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_petra',
  yields: {},
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    PETRA_YIELD_MODIFIER: [{
      ModifierId: 'PETRA_YIELD_MODIFIER',
      Name: 'YieldType',
      Value: 'YIELD_FOOD,YIELD_GOLD,YIELD_PRODUCTION',
    }, { ModifierId: 'PETRA_YIELD_MODIFIER', Name: 'Amount', Value: '2,2,1' }],
  },
}, {
  name: 'Potala Palace',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_potala_palace',
  yields: { YIELD_CULTURE: 2, YIELD_FAITH: 3 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    POTALA_PALACE_DIPLOMATIC_GOVERNMENT_SLOT: [{
      ModifierId: 'POTALA_PALACE_DIPLOMATIC_GOVERNMENT_SLOT',
      Name: 'GovernmentSlotType',
      Value: 'SLOT_DIPLOMATIC',
    }],
  },
}, {
  name: 'Pyramids',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_pyramids',
  yields: { YIELD_CULTURE: 2 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    PYRAMID_ADJUST_BUILDER_CHARGES: [{
      ModifierId: 'PYRAMID_ADJUST_BUILDER_CHARGES',
      Name: 'Amount',
      Value: '1',
    }],
    PYRAMID_GRANT_BUILDERS: [{
      ModifierId: 'PYRAMID_GRANT_BUILDERS',
      Name: 'UnitType',
      Value: 'UNIT_BUILDER',
    }, { ModifierId: 'PYRAMID_GRANT_BUILDERS', Name: 'Amount', Value: '1' }],
  },
}, {
  name: 'Ruhr Valley',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_ruhr_valley',
  yields: {},
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    RUHRVALLEY_ADDPRODUCTIONYIELD: [{
      ModifierId: 'RUHRVALLEY_ADDPRODUCTIONYIELD',
      Name: 'YieldType',
      Value: 'YIELD_PRODUCTION',
    }, { ModifierId: 'RUHRVALLEY_ADDPRODUCTIONYIELD', Name: 'Amount', Value: '20' }],
    RUHR_VALLEY_PRODUCTION_MODIFIER: [{
      ModifierId: 'RUHR_VALLEY_PRODUCTION_MODIFIER',
      Name: 'YieldType',
      Value: 'YIELD_PRODUCTION',
    }, { ModifierId: 'RUHR_VALLEY_PRODUCTION_MODIFIER', Name: 'Amount', Value: '1' }],
  },
}, {
  name: 'Stonehenge',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_stonehenge',
  yields: { YIELD_FAITH: 2 },
  greatPersonPoints: {},
  greatWorksSlots: {},
  modifiers: {
    STONEHENGE_GRANT_PROPHET: [{
      ModifierId: 'STONEHENGE_GRANT_PROPHET',
      Name: 'GreatPersonClassType',
      Value: 'GREAT_PERSON_CLASS_PROPHET',
    }, { ModifierId: 'STONEHENGE_GRANT_PROPHET', Name: 'Amount', Value: '1' }],
    STONEHENGE_ALTERNATE_GRANT_APOSTLE: [{
      ModifierId: 'STONEHENGE_ALTERNATE_GRANT_APOSTLE',
      Name: 'UnitType',
      Value: 'UNIT_APOSTLE',
    }, { ModifierId: 'STONEHENGE_ALTERNATE_GRANT_APOSTLE', Name: 'Amount', Value: '1' }],
  },
}, {
  name: 'Sydney Opera House',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_sydney_opera_house',
  yields: { YIELD_CULTURE: 8 },
  greatPersonPoints: { GREAT_PERSON_CLASS_MUSICIAN: 5 },
  greatWorksSlots: { GREATWORKSLOT_MUSIC: 3 },
  modifiers: {},
}, {
  name: 'Terracotta Army',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_terracotta_army',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_GENERAL: 1 },
  greatWorksSlots: {},
  modifiers: {
    TERRACOTTA_ARMY_ARCHAEOLOGIST_OPEN_BORDERS: [{
      ModifierId: 'TERRACOTTA_ARMY_ARCHAEOLOGIST_OPEN_BORDERS',
      Name: 'AbilityType',
      Value: 'ABILITY_ARCHAEOLOGIST_ENTER_FOREIGN_LANDS',
    }],
    TERRACOTTA_ARMY_LEVEL_UP_UNITS: [{
      ModifierId: 'TERRACOTTA_ARMY_LEVEL_UP_UNITS',
      Name: 'Amount',
      Value: '-1',
    }],
  },
}, {
  name: 'Venetian Arsenal',
  imgSrc: '/icons/wonders/__SIZE__/Icon_building_venetian_arsenal',
  yields: {},
  greatPersonPoints: { GREAT_PERSON_CLASS_ENGINEER: 2 },
  greatWorksSlots: {},
  modifiers: {
    VENETIAN_ARSENAL_EXTRANAVALMELEE: [{
      ModifierId: 'VENETIAN_ARSENAL_EXTRANAVALMELEE',
      Name: 'Tag',
      Value: 'CLASS_NAVAL_MELEE',
    }, { ModifierId: 'VENETIAN_ARSENAL_EXTRANAVALMELEE', Name: 'Amount', Value: '1' }],
    VENETIAN_ARSENAL_EXTRANAVALRANGED: [{
      ModifierId: 'VENETIAN_ARSENAL_EXTRANAVALRANGED',
      Name: 'Tag',
      Value: 'CLASS_NAVAL_RANGED',
    }, { ModifierId: 'VENETIAN_ARSENAL_EXTRANAVALRANGED', Name: 'Amount', Value: '1' }],
    VENETIAN_ARSENAL_EXTRANAVALCARRIER: [{
      ModifierId: 'VENETIAN_ARSENAL_EXTRANAVALCARRIER',
      Name: 'Tag',
      Value: 'CLASS_NAVAL_CARRIER',
    }, { ModifierId: 'VENETIAN_ARSENAL_EXTRANAVALCARRIER', Name: 'Amount', Value: '1' }],
  },
}];
