/* eslint-disable camelcase */
type numericString = string;
type booleanString = 'true' | 'false';

type BuildingType = string;
type Name = string;
type PrereqDistrict = string;
type PurchaseYield = string;
type AdvisorType = string;
type Description = string;
type PrereqTech = string;
type PrereqCivic = string;
type TraitType = string;
type AdjacentResource = string;
type ObsoleteEra = string;
type Quote = string;
type QuoteAudio = string;
type AdjacentDistrict = string;
type YieldType = 'YIELD_CULTURE' | 'YIELD_FAITH' | 'YIELD_FOOD' | 'YIELD_GOLD' | 'YIELD_PRODUCTION' | 'YIELD_SCIENCE'
type GreatPersonClassType = 'GREAT_PERSON_CLASS_ADMIRAL' | 'GREAT_PERSON_CLASS_WRITER' | 'GREAT_PERSON_CLASS_ARTIST' | 'GREAT_PERSON_CLASS_MUSICIAN' | 'GREAT_PERSON_CLASS_ENGINEER' | 'GREAT_PERSON_CLASS_PROPHET' | 'GREAT_PERSON_CLASS_GENERAL' | 'GREAT_PERSON_CLASS_SCIENTIST' | 'GREAT_PERSON_CLASS_MERCHANT'
type GreatWorkSlotType = string
type ThemingBonusDescription = string
type ModifierId = string
type ModifierType = string
type SubjectRequirementSetId = string
type ModifierName = string
type ModifierValue = string

type Building = {
  '-BuildingType': BuildingType,
  '-Name': Name,
  '-PrereqDistrict'?: PrereqDistrict,
  '-PurchaseYield'?: PurchaseYield,
  '-Cost': numericString,
  '-AdvisorType'?: AdvisorType
  '-Housing'?: numericString,
  '-Entertainment'?: numericString,
  '-Capital'?: booleanString,
  '-MaxPlayerInstances'?: numericString,
  '-Description'?: numericString,
  '-PrereqTech'?: PrereqTech,
  '-Maintenance'?: numericString,
  '-CitizenSlots'?: numericString,
  '-OuterDefenseHitPoints'?: numericString,
  '-OuterDefenseStrength'?: numericString,
  '-RequiresAdjacentRiver'?: booleanString,
  '-PrereqCivic'?: PrereqCivic,
  '-TraitType'?: TraitType,
  '-RegionalRange'?: numericString,
  '-EnabledByReligion'?: booleanString,
  '-AdjacentResource'?: AdjacentResource,
  '-MaxWorldInstances'?: numericString,
  '-IsWonder'?: booleanString,
  '-RequiresPlacement'?: booleanString,
  '-AllowsHolyCity'?: booleanString,
  '-ObsoleteEra'?: ObsoleteEra,
  '-Quote'?: Quote,
  '-QuoteAudio'?: QuoteAudio,
  '-RequiresRiver'?: booleanString,
  '-AdjacentDistrict'?: AdjacentDistrict,
  '-MustNotBeLake'?: booleanString,
  '-MustBeAdjacentLand'?: booleanString,
  '-RequiresReligion'?: booleanString,
  '-DefenseModifier'?: numericString,
  '-GrantFortification'?: numericString,
  '-AdjacentToMountain'?: booleanString,
  '-InternalOnly'?: booleanString,
}

type BuildingsDump = {
  GameInfo: {
    Kinds: {} // not needed
    Types: {} // not needed
    BuildingReplaces: {} // not needed
    Buildings: {
      Row: Building[]
    }
    BuildingPrereqs: {} // not needed
    MutuallyExclusiveBuildings: {} // not needed
    Building_ValidFeatures: {} // not needed
    Building_RequiredFeatures: {} // not needed
    Building_ValidTerrains: {} // not needed
    Building_YieldChanges: {
      Row: {
        '-BuildingType': BuildingType,
        '-YieldType': YieldType,
        '-YieldChange': numericString,
      }[]
    }
    Building_YieldsPerEra: {
      Row: {
        '-BuildingType': BuildingType,
        '-YieldType': YieldType,
        '-YieldChange': numericString,
      }
    }
    Building_GreatPersonPoints: {
      Row: {
        '-BuildingType': BuildingType,
        '-GreatPersonClassType': GreatPersonClassType,
        '-PointsPerTurn': numericString
      }[]
    }
    Building_YieldDistrictCopies: {} // not needed
    Building_GreatWorks: {
      Row: {
        '-BuildingType': BuildingType,
        '-GreatWorkSlotType': GreatWorkSlotType,
        '-NumSlots': numericString,
        '-NonUniquePersonYield'?: numericString,
        '-NonUniquePersonTourism'?: numericString,
        '-ThemingUniquePerson'?: booleanString,
        '-ThemingSameObjectType'?: booleanString,
        '-ThemingYieldMultiplier'?: numericString,
        '-ThemingTourismMultiplier'?: numericString,
        '-ThemingBonusDescription'?: ThemingBonusDescription
        '-ThemingSameEras'?: booleanString
        '-ThemingUniqueCivs'?: booleanString
      }[]}
    Modifiers: {
      Row: {
        ModifierId: ModifierId,
        ModifierType: ModifierType,
        Permanent?: booleanString,
        SubjectRequirementSetId?: SubjectRequirementSetId,
        RunOnce?: booleanString,
      }[]
    }
    ModifierArguments: {
      Row: {
        'ModifierId': ModifierId,
        'Name': ModifierName,
        'Value': ModifierValue,
      }[],
    }
    BuildingModifiers: {
      Row: {
        '-BuildingType'?: BuildingType,
        BuildingType?: BuildingType,
        ModifierId: ModifierId
      }[],
    }
    Requirements: {} // not needed
    RequirementArguments: {} // not needed
    RequirementSets: {} // not needed
    RequirementSetRequirements: {} // not needed
  }
}

interface ProcessedWonder {
  name: string
  imgSrc: string
  yields: {
    [modifierId: ModifierId]: number
  }
  greatPersonPoints: {
    [greatPersonType: GreatPersonClassType]: number
  }
  greatWorksSlots: {
    [greatWorkSlotType: GreatWorkSlotType]: number
  }
  modifiers: {}
}
