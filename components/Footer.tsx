function linkTemplate(url, label) {
  return <a href={url} target="_blank" rel="noopener noreferrer">{label}</a>;
}

const copyLinks = {
  hussein: linkTemplate('https://notmybase.com', 'Hussein Duvigneau'),
  fandom: linkTemplate('https://civ6.gamepedia.com/', 'Fandom, Inc.'),
  commons: linkTemplate('https://creativecommons.org/licenses/by-nc-sa/3.0/', 'CC BY-NC-SA 3.0'),
};

export default () => (
  <footer>
    <hr />
    <p>
      Created for entertainment purposes only by {copyLinks.hussein}, 2020. No cookies or data
      collection; enjoy anonymously.
    </p>
    <p>
      Icons provided by {copyLinks.fandom} under {copyLinks.commons}. Game content and materials
      are trademarks and copyrights of their respective publisher and its licensors. All rights
      reserved. This site is not affiliated with the game publisher.
    </p>
  </footer>
);
