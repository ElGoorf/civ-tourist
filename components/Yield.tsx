import Grid from '@material-ui/core/Grid';

const icons = {
  YIELD_GOLD: {
    icon: '/icons/yields/20px-Icon_main_gold.webp',
    label: 'Gold',
  },
  YIELD_SCIENCE: {
    icon: '/icons/yields/20px-Icon_main_science.webp',
    label: 'Science',
  },
  YIELD_CULTURE: {
    icon: '/icons/yields/Icon_Culture.webp',
    label: 'Culture',
  },
  YIELD_FAITH: {
    icon: '/icons/yields/Icon_Faith.webp',
    label: 'Faith',
  },
  GREAT_PERSON_CLASS_ADMIRAL: {
    icon: '/icons/yields/great_people/Icon_Admiral.webp',
    label: 'Admiral',
  },
  GREAT_PERSON_CLASS_SCIENTIST: {
    icon: '/icons/yields/great_people/Icon_Scientist.webp',
    label: 'Scientist',
  },
  GREAT_PERSON_CLASS_GENERAL: {
    icon: '/icons/yields/great_people/Icon_General.webp',
    label: 'General',
  },
  GREAT_PERSON_CLASS_ENGINEER: {
    icon: '/icons/yields/great_people/Icon_Engineer.webp',
    label: 'Engineer',
  },
  GREAT_PERSON_CLASS_MERCHANT: {
    icon: '/icons/yields/great_people/Icon_Merchant.webp',
    label: 'Merchant',
  },
  GREAT_PERSON_CLASS_WRITER: {
    icon: '/icons/yields/great_people/Icon_Writer.webp',
    label: 'Writer',
  },
  GREAT_PERSON_CLASS_MUSICIAN: {
    icon: '/icons/yields/great_people/Mini_Icon_Great_Musician.webp',
    label: 'Musician',
  },
  GREAT_PERSON_CLASS_ARTIST: {
    icon: '/icons/yields/great_people/Mini_Icon_Great_Artist.webp',
    label: 'Artist',
  },
  GREATWORKSLOT_WRITING: {
    icon: '/icons/yields/great_works/18px-Greatwork_writing.webp',
    label: 'Writing',
  },
  GREATWORKSLOT_MUSIC: {
    icon: '/icons/yields/great_works/18px-Greatwork_music.webp',
    label: 'Music',
  },
  GREATWORKSLOT_ART: {
    icon: '/icons/yields/great_works/18px-Greatwork_landscape.webp',
    label: 'Art',
  },
  GREATWORKSLOT_RELIC: {
    icon: '/icons/yields/great_works/16px-Icon_Relic.webp',
    label: 'Relic',
  },
};

interface P {
  perTurn: {
    type: string
    amount: number
  }
}

export default ({ perTurn }: P) => {
  const yieldCopy = icons[perTurn.type];

  return (
    <Grid
      item
      xs={6}
      sm={3}
      style={{
        listStyle: 'none',
        lineHeight: '32px',
        textAlign: 'center',
      }}
    >
      <img
        src={yieldCopy.icon}
        style={{
          verticalAlign: 'sub',
        }}
        alt=""
      />
      {' '}
      {perTurn.amount}
      {' '}
      {yieldCopy.label}
    </Grid>
  );
};

/*
    <Grid
      container
      item
      xs={3}
      direction="row"
      justify="center"
      alignItems="center"
      style={{
        listStyle: 'none',
        lineHeight: '32px',
      }}
    >
      <Grid item xs={6} sm={4}>
        <img
          src={yieldCopy.icon}
          width={19}
          height={19}
          style={{
            verticalAlign: 'sub',
          }}
          alt=""
        />
      </Grid>
      <Grid item xs={6} sm={4}>
        {perTurn.amount}
      </Grid>
      <Grid item xs={12} sm={4}>
        {yieldCopy.label}
      </Grid>
    </Grid>
 */
