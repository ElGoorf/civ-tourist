/*
  ALHAMBRA_MILITARY_GOVERNMENT_SLOT = +1 military policy slot
  BIG_BEN_ECONOMIC_GOVERNMENT_SLOT = +1 economic policy slot
  POTALA_PALACE_DIPLOMATIC_GOVERNMENT_SLOT = +1 diplomatic policy slot
  FORBIDDEN_CITY_WILDCARD_GOVERNMENT_SLOT = +1 wildcard policy slot

  STONEHENGE_GRANT_PROPHET || STONEHENGE_ALTERNATE_GRANT_APOSTLE = free prophet / apostle
  MAHABODHITEMPLE_APOSTLE = 2 free apostles
  HAGIA_SOPHIA_ADJUST_RELIGIOUS_CHARGES = missionaries and apostles +1 religious spread
  MONT_ST_MICHEL_GRANT_MARTYR = all apostles have free Martyr ability

  CRISTOREDENTOR_FULLRELIGIOUSTOURISM = Religeous tourism not diminished by other civilisations who have the Enlightenment civic.
  CRISTOREDENTOR_BEACHTOURISM = 200% tourism from seaside resorts

  BOLSHOI_THEATRE_FREE_CIVICS = 2 random free civis
  OXFORD_UNIVERSITY_FREE_TECHS = 2 random free techs
  BUILDING_BROADWAY_RANDOMCIVICBOOST = 1 free random Atomic Era civic boost
  GREAT_LIBRARY_ANCIENT_CLASSICAL_TECH_BOOSTS = Boosts all Ancient and Classical Era techs

  BIG_BEN_DOUBLE_GOLD = double current treasury
  TERRACOTTA_ARMY_LEVEL_UP_UNITS = All current land units gain a promotion level
  PYRAMID_GRANT_BUILDERS = 1 free builder
  COLOSSUS_GRANT_TRADER = +1 trader unit

  VENETIAN_ARSENAL_EXTRANAVALMELEE || VENETIAN_ARSENAL_EXTRANAVALRANGED || VENETIAN_ARSENAL_EXTRANAVALCARRIER = second naval unit for each one trained
  GREATLIGHTHOUSE_ADJUST_SEA_MOVEMENT || GREATLIGHTHOUSE_ADJUST_EMBARKED_MOVEMENT = +1 movement for all naval units

  GREAT_ZIMBABWE_ADDTRADEROUTE = +1 trade route capacity
  COLOSSUS_ADDTRADEROUTE = +1 trade route capacity

  GREAT_ZIMBABWE_DOMESTICBONUSRESOURCEGOLD || GREAT_ZIMBABWE_INTERNATIONALBONUSRESOURCEGOLD = trade routes from this city get +2 gold for every bonus resource in the city
  RUHRVALLEY_ADDPRODUCTIONYIELD = +20% production in city
  RUHR_VALLEY_PRODUCTION_MODIFIER = +1 production for each mine and quarry in city
  OXFORD_ADDSCIENCEYIELD = +20% science for city
  BROADWAY_ADDCULTUREYIELD = +20% culture to city
  CHICHEN_ITZA_JUNGLE_CULTURE = +2 culture for all rainforest tiles in city
  CHICHEN_ITZA_JUNGLE_PRODUCTION = +1 production for all rainforest tiles in city
  PETRA_YIELD_MODIFIER = + 2 food, +2 gold and +1 production on all desert tiles for home city
  ORACLE_GREATGENERALPOINTS || ORACLE_GREATADMIRALPOINTS || ORACLE_GREATENGINEERPOINTS || ORACLE_GREATMERCHANTPOINTS || ORACLE_GREATSCIENTISTPOINTS ||  ORACLE_GREATWRITERPOINTS = +2 great person points on each relevant district for this city

  HANGING_GARDEN_ADDGROWTH = 15% population growth in all cities
  EIFFEL_TOWER_ADDAPPEAL = +2 appeal to all tiles
  PYRAMID_ADJUST_BUILDER_CHARGES = builders extra charge
  ORACLE_PATRONAGE_FAITH_DISCOUNT = 25% faith discount on Great People
  TERRACOTTA_ARMY_ARCHAEOLOGIST_OPEN_BORDERS = archeologists have free access
*/

interface P {
  modifiers: any
}

export default ({ modifiers }: P) => (
  <ul>
    {/*    {
      (policySlots === 1) && (
        modifiers.ALHAMBRA_MILITARY_GOVERNMENT_SLOT && "Granted a free Military Policy slot."
          || modifiers.BIG_BEN_ECONOMIC_GOVERNMENT_SLOT && "Granted a free Economic Policy slot."
          || modifiers.POTALA_PALACE_DIPLOMATIC_GOVERNMENT_SLOT && "Granted a free Diplomatic Policy slot."
          || modifiers.FORBIDDEN_CITY_WILDCARD_GOVERNMENT_SLOT && "Granted a free Wildcard Policy slot."
      )
    }

    {
      (policySlots > 1) && (
        modifiers.ALHAMBRA_MILITARY_GOVERNMENT_SLOT && "Granted a free military policy slot."
        || modifiers.BIG_BEN_ECONOMIC_GOVERNMENT_SLOT && "Granted a free economic policy slot."
        || modifiers.POTALA_PALACE_DIPLOMATIC_GOVERNMENT_SLOT && "Granted a free diplomatic policy slot."
        || modifiers.FORBIDDEN_CITY_WILDCARD_GOVERNMENT_SLOT && "Granted a free wildcard policy slot."
      )
    } */}
    {modifiers.ALHAMBRA_MILITARY_GOVERNMENT_SLOT && (<li>Granted a free Military Policy slot.</li>)}
    {modifiers.BIG_BEN_ECONOMIC_GOVERNMENT_SLOT && (<li>Granted a free Economic Policy slot.</li>)}
    {modifiers.POTALA_PALACE_DIPLOMATIC_GOVERNMENT_SLOT && (<li>Granted a free Diplomatic Policy slot.</li>)}
    {modifiers.FORBIDDEN_CITY_WILDCARD_GOVERNMENT_SLOT && (<li>Granted a free Wildcard Policy slot.</li>)}

    {modifiers.STONEHENGE_GRANT_PROPHET && modifiers.MAHABODHITEMPLE_APOSTLE && (<li>Granted 2 free Apostles plus a free Prophet (or third Apostle if Prophet already claimed).</li>)}
    {modifiers.STONEHENGE_GRANT_PROPHET && !modifiers.MAHABODHITEMPLE_APOSTLE && (<li>Granted 1 free Prophet, or Apostle if Prophet already claimed.</li>)}
    {!modifiers.STONEHENGE_GRANT_PROPHET && modifiers.MAHABODHITEMPLE_APOSTLE && (<li>Granted 2 free Apostles.</li>)}
    {modifiers.HAGIA_SOPHIA_ADJUST_RELIGIOUS_CHARGES && (<li>Missionaries and Apostles have +1 spread.</li>)}
    {modifiers.MONT_ST_MICHEL_GRANT_MARTYR && (<li>Apostles granted free Martyr ability.</li>)}

    {/* Tourism Boosts */}
    {modifiers.CRISTOREDENTOR_FULLRELIGIOUSTOURISM && (<li>Religious Tourism unaffected by other civilisations' Enlightenment civic.</li>)}
    {modifiers.CRISTOREDENTOR_BEACHTOURISM && (<li>Double tourism from all Seaside Resorts.</li>)}

    {/* Tech and Civic boosts */}
    {modifiers.BOLSHOI_THEATRE_FREE_CIVICS && (<li>Granted 2 free Civics, selected at random.</li>)}
    {modifiers.BUILDING_BROADWAY_RANDOMCIVICBOOST && (<li>Boost for one randomly selected Atomic Era civic.</li>)}
    {modifiers.OXFORD_UNIVERSITY_FREE_TECHS && (<li>Granted 2 free Techs, selected at random.</li>)}
    {modifiers.GREAT_LIBRARY_ANCIENT_CLASSICAL_TECH_BOOSTS && (<li>Boosts to all Ancient and Classical Era techs.</li>)}

    {/* Other One-Time boosts */}
    {modifiers.BIG_BEN_DOUBLE_GOLD && (<li>Doubling of current treasury.</li>)}
    {modifiers.TERRACOTTA_ARMY_LEVEL_UP_UNITS && (<li>All current land units gained a promotion level.</li>)}
    {modifiers.PYRAMID_GRANT_BUILDERS && (<li>Granted 1 free Builder.</li>)}
    {modifiers.COLOSSUS_GRANT_TRADER && (<li>Granted 1 free Trader.</li>)}

    {/* Naval Boosts */}
    {modifiers.VENETIAN_ARSENAL_EXTRANAVALMELEE && (<li>Second Naval Unit for each one trained.</li>)}
    {modifiers.GREATLIGHTHOUSE_ADJUST_SEA_MOVEMENT && (<li>+1 movement for all Naval Units per turn.</li>)}

    {/* Trade boosts */}
    {modifiers.GREAT_ZIMBABWE_ADDTRADEROUTE && modifiers.COLOSSUS_ADDTRADEROUTE && (<li>+2 Trade Route capacity.</li>)}
    {(modifiers.GREAT_ZIMBABWE_ADDTRADEROUTE || modifiers.COLOSSUS_ADDTRADEROUTE) && (<li>+1 Trade Route capacity.</li>)}

    {/* for some wonders, the city they are built in will benefit...: */}
    {modifiers.GREAT_ZIMBABWE_DOMESTICBONUSRESOURCEGOLD && (<li>+2 gold per Trade Route from this city, for every bonus resource in its territory.</li>)}
    {modifiers.RUHRVALLEY_ADDPRODUCTIONYIELD && (<li>+20% Production, and further +1 Production for each mine and quarry belonging to it.</li>)}
    {modifiers.OXFORD_ADDSCIENCEYIELD && (<li>+20% Science.</li>)}
    {modifiers.BROADWAY_ADDCULTUREYIELD && (<li>+20% Culture.</li>)}
    {modifiers.CHICHEN_ITZA_JUNGLE_CULTURE && (<li>+2 Culture and +1 Production for each Rainforest Tile belonging to this city.</li>)}
    {modifiers.PETRA_YIELD_MODIFIER && (<li>+ 2 Food, +2 Gold and +1 Production for each Desert Tile belonging to this city.</li>)}
    {(modifiers.ORACLE_GREATGENERALPOINTS) && (<li>+2 Great Person points for each relevant district.</li>)}

    {/* misc */}
    {modifiers.HANGING_GARDEN_ADDGROWTH && (<li>15% population growth in all cities.</li>)}
    {modifiers.EIFFEL_TOWER_ADDAPPEAL && (<li>+2 appeal to all tiles.</li>)}
    {modifiers.PYRAMID_ADJUST_BUILDER_CHARGES && (<li>All builders granted an extra charge.</li>)}
    {modifiers.ORACLE_PATRONAGE_FAITH_DISCOUNT && (<li>25% faith discount on Great People.</li>)}
    {modifiers.TERRACOTTA_ARMY_ARCHAEOLOGIST_OPEN_BORDERS && (<li>Archaeologists have completely free access over the entire map.</li>)}
  </ul>
);
