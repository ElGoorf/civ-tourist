import Grid from '@material-ui/core/Grid';
import React from 'react';
import WonderCard from './WonderCard';
import wonderList from "../data/cache/wonderList";

export default ({ handleChange }) => (
  <Grid
    container
    direction="row"
    justify="center"
    alignItems="stretch"
    spacing={2}
  >
    {wonderList.map((wonder) => (
      <Grid
        item
        key={wonder.name}
        xs={4}
        sm={3}
        md={2}
        lg={2}
        xl={1}
      >
        <WonderCard
          wonder={wonder}
          onChange={handleChange(wonder.name)}
        />
      </Grid>
    ))}
  </Grid>
);
