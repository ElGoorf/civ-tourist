import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
  root: {
    position: 'relative',
    height: '100%',
    padding: '1px',
    '& .MuiCardActionArea-root': {
      height: '100%',
    },
    '& img': {
      display: 'block',
      width: '100%',
    },
    '& .MuiCheckbox-root': {
      position: 'absolute',
      opacity: 0,
      top: '-10px',
      right: '-10px',
      transitionProperty: 'opacity',
      transitionDuration: '0.5s',
    },
    '&:hover .MuiCheckbox-root': {
      position: 'absolute',
      opacity: 1,
    },
    '& .Mui-checked': {
      opacity: 1,
    },
    '& label': {
      display: 'block',
      textTransform: 'capitalize',
      textAlign: 'center',
      width: '100%',
    },
    '& .MuiGrid-container': {
      height: '100%',
    },
  },
});

interface P {
  wonder: any
  onChange: (e: any) => void
}

export default ({ wonder, onChange }: P) => {
  const [selected, setSelected] = useState(false);

  const toggleSelection = () => {
    const newSelected = !selected;
    onChange(newSelected);
    setSelected(newSelected);
  };

  const classes = useStyles({ selected });

  // 1920+: 136
  // 1280-1919: 188
  // 960-1279: 135
  // 600-959: 118-122
  // ...-599: ...-169

  const allSizes = [
    { upTo: 599, size: 169 },
    { upTo: 959, size: 122 },
    { upTo: 1279, size: 135 },
    { upTo: 1919, size: 188 },
    { size: 137 },
  ];

  const allQueries = allSizes.map(({ upTo }) => `(max-width: ${upTo}px)`).join(', ');
  const allSrcs = allSizes.map(({ size }) => `${wonder.imgSrc.replace('__SIZE__', size)}.webp`);

  const srcSet = allSizes
    .map((img) => {
      const imgUrl = `${wonder.imgSrc.replace('__SIZE__', `${img.size}`)}.webp`;
      const size = img.upTo ? ` ${img.upTo}w` : '';
      return `${imgUrl}${size}`;
    })
    .join(', ');

  return (
    <Card
      variant="outlined"
      onClick={toggleSelection}
      className={classes.root}
    >
      <CardActionArea>
        <Grid
          spacing={0}
          container
          direction="row"
          justify="center"
        >
          <Grid
            item
            style={{ width: '100%' }}
          >
            <picture>
              {/*              <source
                type="image/webp"
                srcSet={srcSet}
                // sizes={allQueries}
                sizes="100%"
              /> */}
              {allSizes.map((img) => (
                <source
                  key={`${img.upTo}-${img.size}`}
                  type="image/webp"
                  media={img.upTo && `(max-width: ${img.upTo}px)`}
                  srcSet={`${wonder.imgSrc.replace('__SIZE__', String(img.size))}.webp`}
                />
              ))}
              <source type="image/png" srcSet={`${wonder.imgSrc.replace('__SIZE__', 'png188')}.png`} />
              <img
                draggable={false}
                src={`${wonder.imgSrc.replace('__SIZE__', 'png188')}.png`}
                alt={wonder.name}
              />
            </picture>
          </Grid>
          <Grid
            item
          >
            <label
              htmlFor={`cb-${wonder.name}`}
            >
              <Typography>{wonder.name}</Typography>
            </label>
          </Grid>
        </Grid>
      </CardActionArea>
      <Checkbox
        id={`cb-${wonder.name}`}
        checked={selected}
      />
    </Card>
  );
};
