import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import React from 'react';
import { Grid } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import wonderList from '../../data/cache/wonderList';
import Yield from '../Yield';
import ExtraBonuses from '../ExtraBonuses';
import YieldSection from './YieldSection';

const yieldOrder = {
  YIELD_SCIENCE: 0, YIELD_CULTURE: 0, YIELD_FAITH: 0, YIELD_GOLD: 0,
};

export default ({ wonderSelections }) => {
  const selectedWonders = Object.values(wonderSelections).filter(Boolean);
  const selectedWondersPercentage = (selectedWonders.length * 100) / wonderList.length;

  const selectedWonderBenefits = (benefitType, init = {}) => wonderList
    .filter((wonder) => wonderSelections[wonder.name])
    .reduce((acc, wonder) => {
      const newAcc = { ...acc };

      Object.entries(wonder[benefitType]).forEach(([yieldType, yieldAmount]) => {
        newAcc[yieldType] = newAcc[yieldType] ? newAcc[yieldType] + yieldAmount : yieldAmount;
      });

      return newAcc;
    }, init);

  const selectedWonderYields = selectedWonderBenefits('yields', yieldOrder);
  const selectedWonderGreatPersonPoints = selectedWonderBenefits('greatPersonPoints');
  const selectedGreatWorksSlots = selectedWonderBenefits('greatWorksSlots');
  const selectedModifiers = selectedWonderBenefits('modifiers');

  return (
    <Paper variant="outlined">
      <Container>
        <Typography
          variant="h2"
          style={{
            fontSize: '2rem',
            textAlign: 'center',
            margin: '10px',
          }}
        >
          You've visited {selectedWonders.length}
          {' '}
          out of
          {' '}
          {wonderList.length}
          {' '}
          wonders (
          {Math.round(selectedWondersPercentage)}
          %)
        </Typography>
        <LinearProgress
          variant="determinate"
          value={selectedWondersPercentage}
        />
        <Typography variant="h2" style={{ fontSize: '2rem', textAlign: 'center', margin: '10px' }}>If your travels were a civilization...</Typography>
      </Container>
      <Grid container>
        <Grid item xs={12} sm={6}>
          <YieldSection
            title="Yield Bonuses"
            desc="Your additional income each turn would be"
            data={selectedWonderYields}
          />
          <YieldSection
            title="Great Work Slots"
            desc="You'd have space for"
            data={selectedGreatWorksSlots}
          />
          <YieldSection
            title="Great Person Points"
            desc="Per turn, you'd receive the following Great Person points"
            data={selectedWonderGreatPersonPoints}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography variant="h3" style={{ fontSize: '2rem', textAlign: 'center' }}>Other Bonuses:</Typography>
          <ExtraBonuses modifiers={selectedModifiers} />
        </Grid>
      </Grid>
    </Paper>
  );
};
