import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Yield from '../Yield';

export default ({ title, desc, data }) => {
  const renderedData = Object.entries(data)
    .map(([gwType, gwAmount]: [string, number]) => (
      <Yield key={gwType} perTurn={{ type: gwType, amount: gwAmount }} />
    ));

  if (renderedData.length === 0) { return null; }

  return (
    <section style={{ margin: '10px' }}>
      <Typography variant="h3" style={{ fontSize: '2rem', textAlign: 'center' }}>{title}</Typography>
      <Typography style={{ textAlign: 'center', marginBottom: '10px' }}>{desc}:</Typography>
      <Grid
        container
        spacing={1}
        direction="row"
        justify="center"
        alignItems="center"
      >
        {
          renderedData
        }
      </Grid>
    </section>
  );
};
