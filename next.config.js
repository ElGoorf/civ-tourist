const withPWA = require('next-pwa');

module.exports = withPWA({
  target: 'serverless',
  pwa: {
    dest: 'public',
    publicExcludes: [
      '!icons/wonders/122',
      '!icons/wonders/135',
      '!icons/wonders/137',
      '!icons/wonders/169',
      '!icons/wonders/png',
      '!icons/wonders/png188',
    ],
  },
});
